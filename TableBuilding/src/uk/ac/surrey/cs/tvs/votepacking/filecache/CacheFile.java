/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The CacheFile is used to retrieve a value from the previous table. We build the overall table by building on top of the previous
 * table. As such, we retrieve the relevant data from the previous table, indexed by the preference data. The rows in the new table
 * will consist of each row in the previous table being copied and modified multiple times, for each different preference. As such,
 * each line is used multiple times, hence the caching. Due to the way the tables are ordered and constructed, we can be certain
 * that if the current entry is not the right entry the next entry is. We can also be certain we will only progress forwards through
 * the previous table.
 * 
 * The actual data is read via a different thread and put into a queue. This class just reads the next value from the queue. If the
 * queue is empty and is awaiting further data, it will block until data has been read in.
 * 
 * @author Chris Culnane
 * 
 */
public class CacheFile {

  /**
   * Size of the cache queue
   */
  private static final int                CACHE_SIZE               = 1000;

  /**
   * The encoded length of an Elliptic Curve point - this is fixed according to ASN.1 encoding for P256 curve
   */
  public static final int                 ENCODE_LENGTH            = 65;

  /**
   * The encoded length of an Elliptic Curve point with compression - this is fixed according to ASN.1 encoding for P256 curve
   */
  public static final int                 ENCODE_LENGTH_COMPRESSED = 33;

  /**
   * The last CacheEntry we used
   */
  private CacheEntry                      lastCacheEntry           = null;

  /**
   * The block size - this is the block size of file in the cache, not the one being generated
   */
  private int                             blockSize;

  /**
   * The data queue we use for storing the actual data. This is filled on a separate thread by the CacheFiller
   */
  private LinkedBlockingQueue<CacheEntry> dataQueue                = new LinkedBlockingQueue<CacheEntry>(CACHE_SIZE);

  /**
   * Thread to run the cacheFiller on
   */
  private Thread                          filler;

  /**
   * Reference to the cacheFiller, should be closed once finished with
   */
  private CacheFiller                     cacheFiller;

  /**
   * Logger
   */
  private static final Logger             logger                   = LoggerFactory.getLogger(CacheFile.class);
  private boolean                         cacheFillerStarted       = false;

  /**
   * @throws FileNotFoundException
   * 
   */
  public CacheFile(File dataFile, int blockSize) throws FileNotFoundException {
    super();

    logger.info("Creating new CacheFile of {} with blockSize: {}", dataFile.getAbsolutePath(), blockSize);
    this.blockSize = blockSize;
    this.cacheFiller = new CacheFiller(dataFile, this.dataQueue, this.blockSize);
  }

  /**
   * Method to start the CacheFiller thread that fills the data queue in the background. If this has not been explictly called when
   * getValue is called it will throw an exception
   */
  public void start() {
    if (!cacheFillerStarted) {
      this.filler = new Thread(this.cacheFiller, "CacheFiller");
      this.filler.setDaemon(true);
      this.filler.start();
      this.cacheFillerStarted = true;
      logger.info("Started CacheFiller thread");
    }
  }

  /**
   * Shuts down the cache filler and empties the queue
   */
  public void close() {
    this.cacheFiller.shutdown();
  }

  /**
   * Gets the value for the specified byte array key (preference array). It will block until data is available.
   * 
   * @param key
   *          byte array of preferences to get data for
   * @return byte array of data relevant to the preference list
   * @throws IOException
   * @throws InterruptedException
   * @throws CacheException
   */
  public byte[] getValue(byte[] key) throws IOException, InterruptedException, CacheException {
    if (!this.cacheFillerStarted) {
      throw new CacheException("CacheFiller not started - call start on CacheFile");
    }
    // Check if the request is for already read data and if so return it
    if (this.lastCacheEntry != null && Arrays.equals(key, this.lastCacheEntry.getKey())) {
      return this.lastCacheEntry.getValue();
    }
    else {
      // Gets the next piece of data, blocking until available if necessary
      this.lastCacheEntry = this.dataQueue.take();
      CacheEntry startSearch = this.lastCacheEntry;

      // Check if it is the appropriate piece of data, if not keep looking - this should not occur
      while (!Arrays.equals(key, this.lastCacheEntry.getKey())) {
        this.lastCacheEntry = this.dataQueue.take();

        // Check we haven't searched the entire cache
        if (Arrays.equals(this.lastCacheEntry.getKey(), startSearch.getKey())) {
          throw new CacheException("Cannot find key in cache");
        }
      }

      // Return the value found
      return this.lastCacheEntry.getValue();
    }
  }
}
