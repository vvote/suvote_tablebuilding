/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import java.util.concurrent.Callable;

import org.bouncycastle.math.ec.ECPoint;

/**
 * PermutationWorker performs the actual work of building the table entry. It is a small piece of work to be run from an executor
 * service. It takes a set of values, including cache values and builds the new value. It returns itself as the result to allow the
 * ConcurrentWriter to output to file.
 * 
 * @author Chris Culnane
 * 
 */
public class PermutationWorker implements Callable<PermutationWorker> {

  /**
   * The spec we are using for this build
   */
  private BuilderSpec spec;

  /**
   * Holds the value of all but the last of the permutations (useful for looking up cached value)
   */
  private byte[]      allButLast;

  /**
   * Holds the value of the last permutation - this is new permutation to add to the previously cache data
   */
  private byte[]      last;

  /**
   * The data we construct for the new permutation
   */
  private byte[]      data;

  /**
   * Cache value from previous table, if it exists, this is the calculated value related to the permutation held in allButLast
   */
  private byte[]      cacheValue;

  /**
   * Constructs a new Permutation worker with the necessary values
   * 
   * @param spec
   *          BuilderSpec to use for the construction
   * @param allButLast
   *          array of all be but the last permutation
   * @param last
   *          array of length 1 that holds just the last permutation
   * @param cacheValue
   *          byte array of value related to allButLast, can be null if no cache exists
   */
  public PermutationWorker(BuilderSpec spec, byte[] allButLast, byte[] last, byte[] cacheValue) {
    super();

    this.spec = spec;
    this.allButLast = allButLast;
    this.last = last;
    this.cacheValue = cacheValue;
  }

  /**
   * Entry point for Callable.
   * 
   * @see java.util.concurrent.Callable#call()
   */
  @Override
  public PermutationWorker call() throws Exception {
    // Prepare output object
    ECPoint result = null;

    // If we have the cache value use it
    if (this.cacheValue != null) {
      // Decode the cache value
      result = this.spec.ecUtils.getParams().getCurve().decodePoint(this.cacheValue);

      // Add the new permutation value to it by looking up the pre-scaled candidate value
      result = result.add(this.spec.getScaledCandidate(this.last[0] - 1, this.spec.getBlockSize() - 1));
    }
    else {
      // We don't have a cache value. Could be the first table or we might not have access to it
      int preference = 1;

      // Need to build values for all preferences - this will be considerably slower as the number of permutations grows
      for (int i = 0; i < this.allButLast.length; i++) {
        if (i == 0) {
          result = this.spec.getScaledCandidate(this.allButLast[i] - 1, preference - 1);
        }
        else {
          result = result.add(this.spec.getScaledCandidate(this.allButLast[i] - 1, preference - 1));
        }

        preference++;
      }

      // Add in the last value.
      if (result == null) {
        result = this.spec.getScaledCandidate(this.last[0] - 1, preference - 1);
      }
      else {
        result = result.add(this.spec.getScaledCandidate(this.last[0] - 1, preference - 1));
      }
    }

    this.data = result.getEncoded();

    // Return ourselves - which will be accessible in the Future object for the ConcurrentWriter
    return this;
  }

  /**
   * Get the array of all but last preferences
   * 
   * @return byte array of all but the last preferences
   */
  public byte[] getAllButLast() {
    return this.allButLast;
  }

  /**
   * Get the byte array of the calculated table entry
   * 
   * @return byte array of an encoded Elliptic Curve point
   */
  public byte[] getData() {
    return this.data;
  }

  /**
   * Get the last preference
   * 
   * @return byte array of length 1 of the last preference
   */
  public byte[] getLast() {
    return this.last;
  }
}
