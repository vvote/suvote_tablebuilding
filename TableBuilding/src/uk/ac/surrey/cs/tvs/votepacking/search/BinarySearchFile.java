/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.search;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Arrays;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.votepacking.sort.ByteArrayComparator;

/**
 * Provides a Binary Search of a PagedMappedByteBuffer and utility methods for returning the found preferences.
 * 
 * @author Chris Culnane
 * 
 */
public class BinarySearchFile {

  /**
   * The PagedMappedByteBuffer that holds the actual reference to the file
   */
  private PagedMappedByteBuffer pmbb;

  /**
   * The file that holds the sorted data we want to search
   */
  private File                  file;

  /**
   * The FileChannel that will be used to read the data from the disk
   */
  private FileChannel           fc;

  /**
   * FileInputStream to obtain a FileChannel from
   */
  private FileInputStream       fis;

  /**
   * long value to hold the previous start point in the binary search
   */
  long                          previousStart;

  /**
   * long value to hold the previous end point the binary search
   */
  long                          previousEnd;

  /**
   * ByteArrayComparator to compare the returned data with the search data
   */
  private ByteArrayComparator   byteCompare = new ByteArrayComparator();

  /**
   * Logger
   */
  private static final Logger   logger      = LoggerFactory.getLogger(BinarySearchFile.class);

  /**
   * Constructs a BinarySearchFile to perform binary searches on the underlying data. Close must be called when finished with to
   * close the relevant file streams.
   * 
   * @param file
   *          File over data to be searched
   * @param dataLength
   *          the length of lines in the file
   * @throws IOException
   * @throws JSONException
   * @throws JSONIOException
   */
  public BinarySearchFile(File file, int dataLength) throws IOException, JSONException, JSONIOException {
    super();

    logger.info("Creating new BinarySearchFile of {} with line length:{}", file.getAbsolutePath(), dataLength);
    this.file = file;
    this.fis = new FileInputStream(file);
    this.fc = this.fis.getChannel();
    this.pmbb = new PagedMappedByteBuffer(this.fc, dataLength);
  }

  /**
   * Perform a binary search on the sorted data file, looking for the specified data. Note the data should be just the portion of
   * the line that is derived from the ECPoint, not the index as well, since we won't normally know what the index is - if we did we
   * wouldn't be searching for it.
   * 
   * @param data
   *          byte array of data to fine
   * @return byte array of permutation related to that data or null if not found
   */
  public byte[] binarySearch(byte[] data) {
    // Initial search is from start:0 to end:length of file
    this.previousStart = 0;
    this.previousEnd = this.file.length();

    // Variable to hold calculated mid point
    long position;

    //Loop until we find the data or reach the end - finding the data returns out, whilst reaching the end breaks
    while (true) {
      // The next search position is half way between the previous start and end point
      position = this.previousStart + ((this.previousEnd - this.previousStart) / 2);
      // If the position is greater than file length - the line length (i.e. it is after the start of the last line) we stop. The
      // reason it is plus two is because the position needs to account for the base zero of the array and the fact it needs to be
      // after the start of the last line.
      

      // Get the next line from position
      byte[] searchData = this.pmbb.getLine(position);

      // Compare the returned line with the search data - we only check the data portion, not the index portion
      int comp = this.byteCompare.compareLimitLength(data, searchData, data.length);

      if (comp < 0) {
        // Less than means the search line is greater, so we want to search below this
        this.previousEnd = position;
      }
      else if (comp == 0) {
        // If it is equal we have found the data, return it
        return searchData;
      }
      else {
        // Means the search line is less than the returned line, search above it next time
        this.previousStart = position;
      }

      // This means we have search throughout the file and cannot find it
      if ((this.previousEnd - this.previousStart) == 1) {
        break;
      }
    }

    // If we get here we haven't found it, so return null
    return null;
  }

  /**
   * Closes the FileChannel and FileInputStream, should be called once searching has finished
   * 
   * @throws IOException
   */
  public void close() throws IOException {
    this.fc.close();
    this.fis.close();
  }

  /**
   * Utility method for converting a line of byte data into just a byte array of preference data
   * 
   * @param line
   *          byte array of the entire line
   * @param blockSize
   *          int of the block size
   * @return
   */
  public static byte[] convertToPlain(byte[] line, int blockSize) {
    return Arrays.copyOfRange(line, line.length - blockSize, line.length);
  }
}
