/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheDataFile;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheEntry;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;
import uk.ac.surrey.cs.tvs.votepacking.sort.ByteMergeSort;
import uk.ac.surrey.cs.tvs.votepacking.sort.DuplicateException;

/**
 * TableBuilder provides methods to build an Elliptic Curve lookup table for VotePacking. The details of VotePacking are provided in
 * the Technical Report and in http://www.computing.surrey.ac.uk/personal/st/S.Schneider/papers/2013/mycrosen2013.pdf
 * 
 * Fundamentally it is a process of homomorphically scaling the plaintext and then combining the scaled values into a single cipher.
 * Following decryption the original preferences are extracted by looking up the combined value in the table constructed by this
 * class. The total number of candidates and the number ciphers being packed determines the table that is built. It is essential
 * that all entries are unique and as such an explicit uniqueness check is performed during the sort. Should that fail new candidate
 * identifiers would need to be selected and the table rebuilt. This would be repeated until a table of unique values is obtained.
 * It should be noted that the probability of a collision occurring is extremely small and as such you shouldn't have to run this
 * more than once.
 * 
 * @author Chris Culnane
 * 
 */
public class TableBuilder {

  /**
   * Default candidate file name, can be overridden
   */
  public static final String  DEFAULT_CANDIDATE_FILE       = "cands.txt";

  /**
   * Extension for temp files generated during table building
   */
  private static final String DATA_FILE_EXT                = ".dat";

  /**
   * Prefix for temp permutation files
   */
  private static final String TEMP_PERMUTATION_FILE_PREFIX = "permutations_block_";

  /**
   * Prefix for temp sub-table files
   */
  private static final String TEMP_SUBTABLE_FILE_PREFIX    = "table_block_";

  /**
   * Filename for temp combined file
   */
  private static final String TEMP_COMBINED_FILENAME       = "combined";

  /**
   * Logger
   */
  private static final Logger logger                       = LoggerFactory.getLogger(TableBuilder.class);

  /**
   * Number of candidates to build a table for
   */
  private int                 numberCandidates;

  /**
   * The blocksize for the table - we will build sub-tables up to this block size 1-> blockSize
   */
  private int                 blockSize;

  /**
   * The directory to output files to
   */
  private File                outputDir;

  /**
   * Pre-calculates the ECPoint multiplications for the different candidate rankings
   */
  private ECPoint[][]         scaledCandidateCache;

  /**
   * Properties file for recording table structure
   */
  private Properties          tableProps                   = new Properties();

  /**
   * Constructs a new instance of a TableBuilder
   * 
   * @param numberOfCandidates
   *          int of the number of candidates
   * @param blockSize
   *          int of the block size (packing size)
   * @param output
   *          String path to output folder
   * @throws IOException 
   */
  public TableBuilder(int numberOfCandidates, int blockSize, String output) throws IOException {
    super();

    logger.info("Creating new TableBuilder instance for {} candidates with blockSize of {} and output to {}", numberOfCandidates,
        blockSize, output);

    this.numberCandidates = numberOfCandidates;
    this.blockSize = blockSize;
    this.outputDir = new File(output);
    
    IOUtils.checkAndMakeDirs(this.outputDir);
    
  }

  /**
   * Builds the table as specified by the appropriate values, using the scaled candidate ids as input. This method creates the sub
   * tables, combines them and sorts them.
   * 
   * @throws IOException
   * @throws DuplicateException
   * @throws TableBuildingException
   */
  private void buildTable(String finalTableName) throws IOException, DuplicateException, TableBuildingException {
    logger.info("Starting buildTable");

    // Create the data files
    File[] dataFiles = new File[this.blockSize];
    int[] blockSizes = new int[this.blockSize];

    for (int i = 0; i < this.blockSize; i++) {
      blockSizes[i] = i + 1;
      dataFiles[i] = new File(this.outputDir, TEMP_SUBTABLE_FILE_PREFIX + (i + 1) + DATA_FILE_EXT);
    }

    // Loops through each data file creating the sub table
    long startTime = System.currentTimeMillis();

    for (int i = 0; i < dataFiles.length; i++) {
      if (i == 0) {
        this.createSubTableFile(blockSizes[i], null);
      }
      else {
        CacheFile subTableCacheFile =new CacheFile(dataFiles[i - 1], blockSizes[i - 1]);
        subTableCacheFile.start();
        this.createSubTableFile(blockSizes[i],subTableCacheFile);
      }
    }

    // This combines the files, pads them all to the same length and compresses the ECPoints
    logger.info("Starting combine pad and compress");
    this.combinePadCompress(dataFiles, blockSizes, new File(this.outputDir, TEMP_COMBINED_FILENAME + DATA_FILE_EXT), this.blockSize);
    logger.info("Finished combine pad and compress");
    logger.info("Starting MergeSort");

    ByteMergeSort.sort(new File(this.outputDir, TEMP_COMBINED_FILENAME + DATA_FILE_EXT), new File(this.outputDir, finalTableName),
        CacheFile.ENCODE_LENGTH_COMPRESSED + this.blockSize, this.blockSize);
    logger.info("Finished MergeSort");

    long endTime = System.currentTimeMillis();
    logger.info("Total Build Time {}", (endTime - startTime));
    this.tableProps.put("TableFile", finalTableName);
  }

  /**
   * Combines the input files, pads them to same length and compresses the ECPoints before outputting the combined data into a
   * single file ready for sorting
   * 
   * @param inputFiles
   *          File array of input files
   * @param inputBlockSizes
   *          int array of block sizes in respective order of the inputFiles
   * @param outputFile
   *          File to save combined output to
   * @param blockSize
   *          int final block size
   * @throws IOException
   */
  private void combinePadCompress(File[] inputFiles, int[] inputBlockSizes, File outputFile, int blockSize) throws IOException {
    logger.info("About to combine/pad/compress files to output: {}", outputFile.getAbsolutePath());
    BufferedOutputStream bos = null;
    try {
      bos = new BufferedOutputStream(new FileOutputStream(outputFile));

      // Create ECUtils to do point operations with
      ECUtils ecUtils = new ECUtils();

      // Create buffers for encoded length and data length
      byte[] valueBuf = new byte[CacheFile.ENCODE_LENGTH];

      // Create padding buffer and fill with zero bytes
      byte[] padding = new byte[blockSize];
      Arrays.fill(padding, (byte) 0);

      // Loop through each file and process the contents
      for (int fileCounter = 0; fileCounter < inputFiles.length; fileCounter++) {
        logger.info("About to process {} with blocksize {}", inputFiles[fileCounter].getAbsolutePath(),
            inputBlockSizes[fileCounter]);
        DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(inputFiles[fileCounter])));

        // Create buffer for permutation data
        byte[] keyBuf = new byte[inputBlockSizes[fileCounter]];

        try {
          // Loop through until EOF
          while (true) {
            // Read values
            dis.readFully(valueBuf);
            dis.readFully(keyBuf);

            // Decode the ECPoint then re-encode with compression
            bos.write(ecUtils.getParams().getCurve().decodePoint(valueBuf).getEncoded(true));

            // Output the permutation
            bos.write(keyBuf);

            // Pad to the fixed length with zeros
            int pad = padding.length - keyBuf.length;
            if (pad > 0) {
              bos.write(padding, 0, padding.length - keyBuf.length);
            }
          }
        }
        catch (EOFException e) {
          logger.info("End of file reached");
        }
        finally {
          dis.close();
        }
      }
    }
    finally {
      logger.info("Closing output file");
      if(bos!=null){
        bos.close();
      }
    }
  }

  /**
   * Creates the permutations files - these are files that list all the possible permutations of candidates for tables between 1 and
   * blockSize
   * 
   * @throws IOException
   */
  private void createPermutationFiles() throws IOException {
    // Create a new Permutation file for each block size
    logger.info("Starting permutation file generation");

    for (int i = 0; i < this.blockSize; i++) {
      logger.info("Creating Permutation file for {} in {} packing.", (i + 1), this.numberCandidates);

      BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(new File(this.outputDir,
          TEMP_PERMUTATION_FILE_PREFIX + (i + 1) + DATA_FILE_EXT)));
      TVSPermutationGenerator permGen = new TVSPermutationGenerator(this.numberCandidates, i + 1, 1, this.numberCandidates);

      // Loop through permutation generator, writing out permutations
      while (permGen.hasMore()) {
        int[] permArray = permGen.getNextPermutation();

        // We write the permutation individually to ensure each one is a separate byte
        for (int perm : permArray) {
          bos.write(perm);
        }
      }
      bos.close();
    }

    logger.info("Finished permutation file generation");
  }

  /**
   * Calculates and stores in memory the CandidateIdentifier scalings. This improves efficiency since it saves us recalculating
   * point multiplications and allows us to just do point additions when building the table.
   * 
   * @param candidateIdentifiers
   *          Array of ECPoints of candidate identifiers
   */
  private void createScaledCandidateCache(ECPoint[] candidateIdentifiers) {
    logger.info("Creating scaled Candidate Identifiers");

    // Create new array of ECPoints to store scaled candidate identifiers in
    this.scaledCandidateCache = new ECPoint[candidateIdentifiers.length][this.blockSize];

    for (int candCounter = 0; candCounter < candidateIdentifiers.length; candCounter++) {
      for (int blockCounter = 0; blockCounter < this.blockSize; blockCounter++) {
        this.scaledCandidateCache[candCounter][blockCounter] = candidateIdentifiers[candCounter].multiply(BigInteger
            .valueOf(blockCounter + 1));
      }
    }

    logger.info("Finished creating scaled Candidate Identifiers");
  }

  /**
   * Creates a sub table file for a single size of packing
   * 
   * @param blockSize
   *          int block size to create a sub table for
   * @param cache
   *          CacheFile to read previous data from, or null if no cache is available
   * @throws TableBuildingException
   */
  private void createSubTableFile(int blockSize, CacheFile cache) throws TableBuildingException {
    logger.info("Starting SubTable Creation for Table of blockSize {}", blockSize);

    BufferedOutputStream bos = null;

    try {
      // Create output stream
      bos = new BufferedOutputStream(new FileOutputStream(new File(this.outputDir, TEMP_SUBTABLE_FILE_PREFIX + blockSize
          + DATA_FILE_EXT)));

      // Create a ConcurrentWriter
      ConcurrentWriter cw = new ConcurrentWriter(bos);
      Thread writerThread = new Thread(cw, "ConcurrentWriter");
      writerThread.start();

      BuilderSpec spec = new BuilderSpec(this.scaledCandidateCache, blockSize);

      // Create an executor service to run the PermutationWorkers in
      ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 2);

      // Create a new CacheDataFile to read the permutation list from
      CacheDataFile data = new CacheDataFile(new File(this.outputDir, TEMP_PERMUTATION_FILE_PREFIX + blockSize + DATA_FILE_EXT),
          blockSize - 1);
      data.start();
      CacheEntry ce;

      // Loop through permutations to calculate
      logger.info("Starting Table Entry Generation");

      while ((ce = data.getNextValue()) != null) {
        // If the cache isn't null get the data and pass it to the permutation worker
        if (cache != null) {
          try {
            // Add to the futures queue
            cw.addToFutureWriteQueue(exec.submit(new PermutationWorker(spec, ce.getKey(), ce.getValue(),
                cache.getValue(ce.getKey()))));
          }
          catch (Exception e) {
            // we catch and re-throw because otherwise they are suppressed
            throw new TableBuildingException(e);
          }
        }
        else {
          // No cache so submit without cache value
          cw.addToFutureWriteQueue(exec.submit(new PermutationWorker(spec, ce.getKey(), ce.getValue(), null)));
        }

      }

      logger.info("Calling Shutdown on Executor");
      exec.shutdown();
      logger.info("Waiting for Shutdown");
      exec.awaitTermination(100000, TimeUnit.MILLISECONDS);
      logger.info("Shutdown completed");
      cw.forceDrain();

      // Wait for any last write to complete
      cw.checkWaitLastWrite();

      // Interrupt the writer to close it down
      writerThread.interrupt();
      logger.info("Writer shutdown");

      if (cache != null) {
        logger.info("Closing Cache");
        cache.close();
      }
    }
    catch (InterruptedException | ExecutionException | IOException e) {
      logger.error("Exception building table", e);
      throw new TableBuildingException("Exception whilst building table", e);
    }
    finally {
      if (bos != null) {
        try {
          bos.close();
        }
        catch (IOException e) {
          throw new TableBuildingException("IOException when closing stream", e);
        }
      }
    }
  }

  /**
   * Utility method to process candidates IDs and generate tables
   * 
   * @param candidateIDPath
   *          String path to candidate IDs
   * @throws IOException
   * @throws TableBuildingException
   * @throws DuplicateException
   */
  public void doAll(String candidateIDPath, String outputFilename) throws IOException, DuplicateException, TableBuildingException {
    this.createPermutationFiles();

    ECPoint[] candIDS = TableBuilder.readCandidateIDsFromFile(candidateIDPath);
    this.tableProps.put("PlaintextCandidateIDs", candidateIDPath);
    this.createScaledCandidateCache(candIDS);
    this.buildTable(outputFilename);
  }

  /**
   * Utility method for creating sample Candidate ID and storing them into the default file. In production we would expect the
   * candidates ID to be generated externally.
   * 
   * @param number
   *          number of candidates
   * @throws IOException
   */
  public static void createSampleCandidates(int number) throws IOException {
    createSampleCandidates(number, TableBuilder.DEFAULT_CANDIDATE_FILE);
  }

  /**
   * Utility method for creating sample Candidate IDs and storing them into a file. In production we would expect the candidates IDs
   * to be generated externally.
   * 
   * @param number
   *          number of candidates
   * @param filePath
   *          The output file
   * @throws IOException
   */
  public static void createSampleCandidates(int number, String filePath) throws IOException {
    logger.info("Creating {} sample candidate IDs and storing them in {}", number, filePath);
    // Create the output file structure and content.
    JSONArray plaintext = new JSONArray();
    try {
      ECUtils ecUtils = new ECUtils();
      // Get the random candidate id, store it in plain text and store it encrypted.
      for (int i = 0; i < number; i++) {
        ECPoint candidateID = ecUtils.getRandomValue();
        plaintext.put(ECUtils.ecPointToJSON(candidateID));
      }

      // Write the data to the files.
      IOUtils.writeJSONToFile(plaintext, filePath);
    }
    catch (JSONException e) {
      logger.error("Exception generating CandidateIDs", e);
      throw new IOException(e);
    }
    catch (JSONIOException e) {
      logger.error("Exception generating CandidateIDs", e);
      throw new IOException(e);
    }

  }

  /**
   * Main entry point the table building.
   * 
   * @param args
   *          Command line arguments.
   * @throws IOException
   * @throws DuplicateException
   * @throws TableBuildingException
   * @throws InterruptedException
   */
  public static void main(String[] args) throws IOException, DuplicateException, TableBuildingException {
    if (args.length == 4 || args.length == 5) {
      if (args.length == 4) {
        TableBuilder.createSampleCandidates(Integer.parseInt(args[0]));
      }
      else {
        File candidateFile = new File(args[3]);
        if (!candidateFile.exists()) {
          TableBuilder.createSampleCandidates(Integer.parseInt(args[0]), args[3]);
        }
      }

      TableBuilder builder = new TableBuilder(Integer.parseInt(args[0]), Integer.parseInt(args[1]), args[2]);

      if (args.length == 4) {
        builder.doAll(TableBuilder.DEFAULT_CANDIDATE_FILE, args[3]);

      }
      else {
        builder.doAll(args[3], args[4]);

      }

    }
    else {
      System.out.println("Usage:");
      System.out.println("\t TableBuilder numberOfCandidates blockSize outputFolder finalTableName:");
      System.out.println("\t TableBuilder numberOfCandidates blockSize outputFolder candidateFile finalTableName:");
      System.out.println("\n");
      System.out.println("If no candidateFile is specified a new one will always be generated. If it is specified");
      System.out.println("a new one will only be generated if it doesn't already exist. If a file exists and is ");
      System.out.println("explicitly specified it is the responsibility of the user to ensure it has the appropriate");
      System.out.println("number of candidateID values within it.");
    }
  }

  /**
   * Reads candidate IDs from a file and puts them into an array of ECPoints. Each candidate ID should be on a separate line.
   * 
   * @param filepath
   *          String filepath of candidate ID file
   * @return array of ECPoints, one for each candidate ID
   * @throws IOException
   */
  public static ECPoint[] readCandidateIDsFromFile(String filepath) throws IOException {
    logger.info("Reading candidate IDs from {}", filepath);
    ECUtils ecUtils = new ECUtils();
    try {
      JSONArray arr = IOUtils.readJSONArrayFromFile(filepath);
      ECPoint[] ecArr = new ECPoint[arr.length()];
      for (int i = 0; i < arr.length(); i++) {
        ecArr[i] = ecUtils.pointFromJSON(arr.getJSONObject(i));
      }
      return ecArr;
    }
    catch (JSONException e) {
      logger.error("JSONException reading CandidateIDs", e);
      throw new IOException(e);
    }
    catch (JSONIOException e) {
      logger.error("JSONException reading CandidateIDs", e);
      throw new IOException(e);
    }

    // BufferedReader candidates = null;
    //
    // try {
    // candidates = new BufferedReader(new FileReader(filepath));
    // ArrayList<ECPoint> cands = new ArrayList<ECPoint>();
    // String line;
    //
    // while ((line = candidates.readLine()) != null) {
    // cands.add(ecUtils.getParams().getCurve().decodePoint(IOUtils.decodeData(EncodingType.BASE64, line)));
    // }
    //
    // return cands.toArray(new ECPoint[0]);
    // }
    // finally {
    // if (candidates != null) {
    // candidates.close();
    // }
    // }
  }
}
