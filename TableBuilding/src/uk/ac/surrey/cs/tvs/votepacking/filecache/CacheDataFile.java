/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is similar to the CacheFile except it is a one time read of a data file. It is to avoid possible IO lags when processing a
 * lot of data that might potentially block whilst processing takes place. This pre-loads a queue of data into memory to maximise
 * throughput.
 * 
 * @author Chris Culnane
 * 
 */
public class CacheDataFile {

  /**
   * Underlying DataInputStream to read the data from - this will be passed to the CacheDataFiller
   */
  private DataInputStream                 dis;

  /**
   * LinkedBlockingQueue to place the cached data into
   */
  private LinkedBlockingQueue<CacheEntry> dataQueue          = new LinkedBlockingQueue<CacheEntry>(1000);

  /**
   * Thread to run the CacheDataFiller on
   */
  private Thread                          filler;

  /**
   * Marker set to indicate the end of the file has been reached by the filler
   */
  private boolean                         EOF                = false;

  /**
   * Logger
   */
  private static final Logger             logger             = LoggerFactory.getLogger(CacheDataFile.class);

  /**
   * int variable to hold sub block size
   */
  private int                             subBlockSize;
  private boolean                         cacheFillerStarted = false;

  /**
   * Construct a new CacheDataFile with the file and subBlockSize specified. The subBlockSize is the minimum amount of data held in
   * memory. Having constructed the CacheDataFile start() must be called to start the CacheDataFiller, otherwise an exception will
   * be thrown when calling getNextValue
   * 
   * @param dataFile
   *          File to read data from
   * @param subBlockSize
   *          Minimum amount of data held in memory.
   * @throws FileNotFoundException
   */
  public CacheDataFile(File dataFile, int subBlockSize) throws FileNotFoundException {
    super();
    this.subBlockSize = subBlockSize;
    logger.info("Creating new CacheDataFile of {} with block size {}", dataFile.getAbsolutePath(), subBlockSize);
    this.dis = new DataInputStream(new BufferedInputStream(new FileInputStream(dataFile)));
  }

  /**
   * Method to start the CacheDataFile thread that fills the data queue in the background. If this has not been explictly called
   * when getNextValue is called an exception will be thrown
   */
  public void start() {
    if (!this.cacheFillerStarted) {
      this.filler = new Thread(new CacheDataFiller(this.dis, this.dataQueue, subBlockSize), "CacheDataFiller");
      this.filler.setDaemon(true);
      this.filler.start();
      this.cacheFillerStarted = true;
      logger.info("Started new CacheDataFiller Thread");
    }
  }

  /**
   * Gets the next value from the data queue, blocking if necessary for data to be read in
   * 
   * @return CacheEntry of the next value in the file or null if EOF
   * @throws InterruptedException
   * @throws IOException
   */
  public CacheEntry getNextValue() throws InterruptedException, IOException {
    // Start filler thread if it hasn't already been started
    if (!cacheFillerStarted) {
      throw new IOException("CacheDataFiller has not been started");
    }

    // return null if at EOF
    if (this.EOF) {
      return null;
    }

    // Get next entry, blocking until available
    CacheEntry next = this.dataQueue.take();

    // Check if it equals the last entry and set the EOF flag if it is
    if (next.isEOF()) {
      this.EOF = true;

      return null;
    }

    return next;
  }
}
