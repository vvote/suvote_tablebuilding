/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

/**
 * A permutation generator for generating the different permutations of candidate IDs in blocks of a pre-determined size. For
 * example, it can generate all permutations of the number between 1 and 20 in blocks of six, e.g [2,4,5,10,19,15]. This would be
 * referred to a 6 in 20 table.
 * 
 * The min and max values allow partial permutations to be built, possibly of use in a multi-threaded environment. The min and max
 * only act on the first integer in position zero. However, this still allows the permutation generation to be divided up into
 * chunks of work.
 * 
 * It works by counting up from the lowest possible value, incrementing the last digit each time and then checking for uniqueness.
 * Uniqueness is defined as each number only appearing once. If it isn't unique we increment again until we have a unique value.
 * Imagine a block of size 3 (r=3), with 6 candidates (n=6). The following would be created:
 * 
 * Initialise [1,2,3] - initial permutation created [1,2,2] - constructor resets last value so next permutation is correct
 * 
 * getNextPermutationCalls [1,2,3] [1,2,4] [1,2,5] [1,2,6] [1,3,2] - 2 is the first number that complies with uniqueness [1,3,4] -
 * likewise with 4 . . . [6,5,4]
 * 
 * @author Chris Culnane
 * 
 */
public class TVSPermutationGenerator {

  /**
   * n is the number of candidates
   */
  private int   n;

  /**
   * r is the block size (1, ..., 6)
   */
  private int   r;

  /**
   * min is an integer indicating the initial value of the integer in position zero
   */
  private int   min;

  /**
   * max is the max value of the integer in position zero
   */
  private int   max;

  /**
   * last permutation generated
   */
  private int[] permutation;

  /**
   * Constructor for TVSPermutationGenerator, sets n, r and min and max.
   * 
   * @param n
   *          number of candidates
   * @param r
   *          block size
   * @param min
   *          start point of left most value
   * @param max
   *          end of left most value
   */
  public TVSPermutationGenerator(int n, int r, int min, int max) {
    super();

    this.n = n;
    this.r = r;
    this.max = max;
    this.min = min;

    // Create a permutation of the appropriate size
    this.permutation = new int[r];

    // Set the minimum value
    this.permutation[0] = this.min;

    // Fill the rest of the array with unique values
    for (int i = 1; i < r; i++) {
      this.permutation[i] = 1;// min+i;
      while (!this.checkUnique(i)) {
        this.incrementPermutation(i);
      }
    }

    // Set last value to be one less so the first call to getNextPermutation creates the first valid permutation
    this.permutation[r - 1]--;
  }

  /**
   * Checks if the change we have made to a single column results in a unique permutation. This assumes that the starting array was
   * unique and therefore only checks if the column we have changed impacts on that. This is for efficiency, otherwise we would have
   * to check the columns against each other.
   * 
   * @param col
   *          the index of the column to check
   * @return true if this is unique, false if not
   */
  private boolean checkUnique(int col) {
    for (int i = 0; i < this.r - 1; i++) {
      if (i != col) {
        if (this.permutation[i] == this.permutation[col] || this.permutation[col] == 0) {
          return false;
        }
      }
    }

    return true;
  }

  /**
   * Gets the next permutation by first incrementing the last permutation and returning the array of values
   * 
   * @return array of integers containing permutation
   */
  public int[] getNextPermutation() {
    // Increment the last index
    this.incrementPermutation(this.r - 1);

    return this.permutation;
  }

  /**
   * Check if there are more permutations to generate
   * 
   * @return boolean, true if there are more permutations, false if not
   */
  public boolean hasMore() {
    return this.moreToGenerate();
  }

  /**
   * Increments the permutation, adjusting multiple columns if the result is not unique.
   * 
   * @param col
   *          the column to increment
   */
  private void incrementPermutation(int col) {
    if (col == 0) {
      if (this.permutation[col] < this.max) {
        this.permutation[col]++;
      }
    }
    else {
      if (this.permutation[col] == this.n) {
        this.permutation[col] = 0;
        this.incrementPermutation(col - 1);
        while (!this.checkUnique(col - 1)) {
          this.incrementPermutation(col - 1);
        }
        while (!this.checkUnique(col)) {
          this.incrementPermutation(col);
        }
      }
      else {
        this.permutation[col]++;
        while (!this.checkUnique(col)) {
          this.incrementPermutation(col);
        }
      }
    }
  }

  /**
   * Checks whether there are more to generate. The maximum value is the set of [n, n-1,n-2,n-3,...] therefore we just check if all
   * columns have reached their max.
   * 
   * @return true if there are, false if not
   */
  private boolean moreToGenerate() {
    if (this.permutation[0] < this.max) {
      return true;
    }
    else {
      int tempMax = this.n;

      if (this.max == this.n) {
        tempMax--;
      }
      for (int i = 1; i < this.permutation.length; i++) {
        if (this.permutation[i] < tempMax) {
          return true;
        }

        tempMax--;

        if (tempMax == this.max) {
          tempMax--;
        }
      }
    }

    return false;
  }
}
