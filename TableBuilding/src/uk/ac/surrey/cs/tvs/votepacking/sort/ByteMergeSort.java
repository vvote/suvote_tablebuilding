/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.PriorityQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.votepacking.search.BinarySearchFile;

/**
 * Merge sort for byte data. Performs a merge sort on a unlimited size file by breaking it up into chunks and sorting those in
 * memory and storing to file. The files are then reconstructed in a sorted order. Note: unlimited is not strictly unlimited, there
 * are limits on the number of file descriptors and theoretically if you divided the file into enough chunks, even a single line
 * from each file could be greater than the available memory. However, such files would needs to be larger than most hard disks
 * available and way beyond feasible in terms of processing on a single machine.
 * 
 * @author Chris Culnane
 * 
 */
public class ByteMergeSort {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(ByteMergeSort.class);

  /**
   * Sorts a byte file saving the sorted contents to output. The lineLength specifies the conceptual lines within the byte data
   * 
   * @param input
   *          File to be sorted
   * @param output
   *          File to write sorted output to
   * @param lineLength
   *          int of the number of bytes in the conceptual line
   * @param blockSize
   *          int of the block size
   * @throws IOException
   * @throws DuplicateException
   */
  public static void sort(File input, File output, int lineLength, int blockSize) throws IOException, DuplicateException {
    logger.info("About to sort {} into {} with linelength{} and blocksize {}", input.getAbsolutePath(), output.getAbsolutePath(),
        lineLength, blockSize);

    // Create reader for main file
    DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(input)));

    // This will hold the references to the subsorted files
    ArrayList<TempMergeFile> tempMergeFiles = new ArrayList<TempMergeFile>();

    // Array to hold the arrays to do the in-memory subsort
    ArrayList<byte[]> inMemorySort = new ArrayList<byte[]>();

    // Setup threshold - how big each subsort is. I've currently fixed this at 60mb, I've tried dynamic allocation, but the
    // performance was worse. The bottleneck is in file i/o.
    int inMemoryByteCount = 0;
    int inMemoryByteThreshold = 1048576 * 60;

    try {
      while (true) {
        byte[] buf = new byte[lineLength];
        dis.readFully(buf);

        inMemorySort.add(buf);
        inMemoryByteCount = inMemoryByteCount + lineLength;

        // If we are over the threshold, sort the in-memory data and write it out to a temp file
        if (inMemoryByteCount > inMemoryByteThreshold) {
          Collections.sort(inMemorySort, new ByteArrayComparator());
          writeToFile(inMemorySort, tempMergeFiles, lineLength);

          // Reset the counter
          inMemoryByteCount = 0;
        }
      }
    }
    catch (EOFException e) {
      // End of data
    }
    finally {
      dis.close();
    }

    // We've reached the end of the file, now sort and write out the remaining data that is in-memory
    if (inMemorySort.size() > 0) {
      Collections.sort(inMemorySort, new ByteArrayComparator());
      writeToFile(inMemorySort, tempMergeFiles, lineLength);

      // Reset the counter
      inMemoryByteCount = 0;
    }

    logger.info("Finished in-memory sort and file division");

    // Create a PriorityQueue containing a reference to each subfile. Pass the custom comparator which will sort those files based
    // on the top line of each file
    PriorityQueue<TempMergeFile> pq = new PriorityQueue<TempMergeFile>(Math.max(1, tempMergeFiles.size()),
        new TempMergeFileComparator());

    // Add all the references to the temp files, this will sort them at the same time as part of the PriorityQueue functionality
    pq.addAll(tempMergeFiles);
    TempMergeFile current;

    // Create an output file for the sorted output
    BufferedOutputStream bos = null;

    try {
      bos = new BufferedOutputStream(new FileOutputStream(output));

      byte[] lastdata = null;

      // Loop through until the PriorityQueue is empty (once all data from all files has been read). The poll method removes the top
      // most file
      while ((current = pq.poll()) != null) {
        // If the current line is null we have reached the end of that file, do nothing, since we have already removed it from the
        // PriorityQueue
        if (current.getCurrentData() != null) {
          if (lastdata != null) {
            if (ByteArrayComparator.compareSubArrays(lastdata, current.getCurrentData(), lineLength - blockSize) == 0) {
              logger.error("Duplicate found of {} and {}", BinarySearchFile.convertToPlain(lastdata, blockSize),
                  BinarySearchFile.convertToPlain(current.getCurrentData(), blockSize));
              throw new DuplicateException("Duplicate record found in table");
            }
          }

          // We have to take a copy otherwise the underlying byte array object in the TempMergeFile is updated following the next
          // read operation
          lastdata = Arrays.copyOf(current.getCurrentData(), current.getCurrentData().length);

          // Write out the current line to the output file
          bos.write(current.getCurrentData());

          // Get the next line from this file, if it is null we have reached the end of the file, so do nothing if it is not null
          // place it back into the PriorityQueue which will automatically sort it into the correct location
          if (current.useCurrentData() != null) {
            pq.add(current);
          }
        }
      }
    }
    finally {
      if (bos != null) {
        bos.close();
      }
    }

    logger.info("Finished merging files");
  }

  /**
   * Provides a utility method for the java merge sort to write the contents of a subsort to a file. This has been branched off into
   * a separate method to try and improve performance.
   * 
   * @param tempOutputFolder
   *          the folder where this file should be saved
   * @param inMemorySort
   *          the array of arrays to be written
   * @param tempMergeFiles
   *          the array of subfiles that we want to add the created file to
   * @throws IOException
   *           if there is an error writing to the file
   */
  private static void writeToFile(ArrayList<byte[]> inMemorySort, ArrayList<TempMergeFile> tempMergeFiles, int lineLength)
      throws IOException {
    // Create a temp file with suffix and prefix
    File tempOutputFile = File.createTempFile("tvsmerge", ".tvs");

    // Create a file writer and buffer
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(tempOutputFile));

    // Iterate through the arrays in the array and write them out.
    Iterator<byte[]> it = inMemorySort.iterator();
    while (it.hasNext()) {
      bos.write(it.next());
    }

    // Flush and close the file
    bos.close();

    // Add a reference to this new sub merge file and clear the in-memory array.
    tempMergeFiles.add(new TempMergeFile(tempOutputFile, lineLength));
    inMemorySort.clear();
  }
}
