/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import org.bouncycastle.math.ec.ECPoint;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;

/**
 * Holds shared data objects and parameters for the table building. This allows the data to only be held once in memory and easily
 * passed between processes and threads.
 * 
 * @author Chris Culnane
 * 
 */
public class BuilderSpec {

  /**
   * The pre-calculated scaled candidates. The first index is the candidate index 0-(n-1), the second is the preference 0-(r-1)
   */
  private ECPoint[][]  scaledCandidateCache;

  /**
   * The block size (r) value
   */
  private int          blockSize;

  /**
   * Elliptic Curve utilities, these will be used else where to decode points
   */
  /* package */ECUtils ecUtils = new ECUtils();

  /**
   * Constructs a BuilderSpec with the scaledCandidate values and the block size
   * 
   * @param scaledCandidateCache
   *          ECPoint array of arrays with pre-scaled candidate identifiers
   * @param blockSize
   *          int of the block size
   */
  public BuilderSpec(ECPoint[][] scaledCandidateCache, int blockSize) {
    super();

    this.scaledCandidateCache = scaledCandidateCache;
    this.blockSize = blockSize;
  }

  /**
   * Get the block size
   * 
   * @return int of the block size
   */
  public int getBlockSize() {
    return this.blockSize;
  }

  /**
   * Get the shared ECUtils object
   * 
   * @return ECUtils object
   */
  public ECUtils getECUtils() {
    return this.ecUtils;
  }

  /**
   * Get one of the scaled candidate identifiers, passing in the candidate index and the preference (1 to blocksize) that is
   * required.
   * 
   * @param candidateIndex
   *          integer of the candidate identifier
   * @param preferenceIndex
   *          integer of the preference
   * @return ECPoint of the pre-scaled candidate identifier
   */
  public ECPoint getScaledCandidate(int candidateIndex, int preferenceIndex) {
    return this.scaledCandidateCache[candidateIndex][preferenceIndex];
  }
}
