/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import java.util.Comparator;

/**
 * Comparator for comparing byte array values with each other. Contains the required compare method as well as utility method
 * compareLimitLength that only compares up to a certain point. This is useful for comparing byte array that contain both data and
 * index values.
 * 
 * @author Chris Culnane
 * 
 */
public class ByteArrayComparator implements Comparator<byte[]> {

  /**
   * Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first argument is less
   * than, equal to, or greater than the second.
   * 
   * @param o1
   *          the first object to be compared.
   * @param o2
   *          the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the
   *         second.
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(byte[] o1, byte[] o2) {
    // If the length is different they clearly aren't equal - longer is bigger
    int lenDif = o1.length - o2.length;

    if (lenDif != 0) {
      return lenDif;
    }
    else {
      // Step through the arrays checking each value, if any aren't equal which ever is larger is bigger
      for (int i = 0; i < o1.length; i++) {
        int dif = o1[i] - o2[i];

        if (dif != 0) {
          return dif;
        }
      }
    }

    // If we get here it means they are equal
    return 0;
  }

  /**
   * Compares one byte array with another, where the length of the second is limited by lengthOfo2.
   * 
   * Fundamentally this is the same as compare, just with a limited length.
   * 
   * @param o1
   *          the first byte array to compare
   * @param o2
   *          the second byte array to compare
   * @param lengthOfo2
   *          the length of o2 to include in the compare
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the
   *         second.
   */
  public int compareLimitLength(byte[] o1, byte[] o2, int lengthOfo2) {
    // If the length is different they clearly aren't equal - longer is bigger
    int lenDif = o1.length - lengthOfo2;

    if (lenDif != 0) {
      return lenDif;
    }
    else {
      // Step through the arrays checking each value, if any aren't equal which ever is larger is bigger
      for (int i = 0; i < o1.length; i++) {
        int dif = o1[i] - o2[i];

        if (dif != 0) {
          return dif;
        }
      }
    }

    // If we get here it means they are equal
    return 0;
  }

  /**
   * Compares one byte array with another, where the length of both byte arrays is limited. If the length of both arrays is less
   * than the specified length the arrays will be compared as is. If one array is shorter than the other, irrespective of the
   * overall length, it will be considered to be smaller.
   * 
   * Fundamentally this is the same as compare, just with a limited length.
   * 
   * @param o1
   *          the first byte array to compare
   * @param o2
   *          the second byte array to compare
   * @param lengthToCompare
   *          the length of o2 to include in the compare
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the
   *         second.
   */
  public static int compareSubArrays(byte[] o1, byte[] o2, int length) {
    // If the length is different they clearly aren't equal - longer is bigger
    if (o1.length < length || o2.length < length) {
      int lenDif = o1.length - o2.length;

      if (lenDif != 0) {
        return lenDif;
      }
    }
    else {
      // Step through the arrays checking each value, if any aren't equal which ever is larger is bigger
      // The length of the check is determined by either the specified length or the length of both arrays, if smaller
      for (int i = 0; i < Math.min(length, o1.length); i++) {
        int dif = o1[i] - o2[i];

        if (dif != 0) {
          return dif;
        }
      }
    }

    // If we get here it means they are equal
    return 0;
  }
}
