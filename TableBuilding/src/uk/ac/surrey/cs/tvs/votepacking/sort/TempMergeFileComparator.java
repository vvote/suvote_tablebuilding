/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import java.util.Comparator;

/**
 * Comparator for comparing different TempMergeFiles when merging them back together. This is just a comparison of the current byte
 * array data each merge file holds. As such, we just call the ByteArrayComparator on each TempMergeFile.
 * 
 * @author Chris Culnane
 * 
 */
public class TempMergeFileComparator implements Comparator<TempMergeFile> {

  /**
   * ByteArrayComparator to use to compare the merge file
   */
  private ByteArrayComparator byteCompare = new ByteArrayComparator();

  /**
   * Compares its two arguments for order. Returns a negative integer, zero, or a positive integer as the first argument is less
   * than, equal to, or greater than the second.
   * <p>
   * 
   * @param o1
   *          the first object to be compared.
   * @param o2
   *          the second object to be compared.
   * @return a negative integer, zero, or a positive integer as the first argument is less than, equal to, or greater than the
   *         second.
   * 
   * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
   */
  @Override
  public int compare(TempMergeFile file1, TempMergeFile file2) {
    return this.byteCompare.compare(file1.getCurrentData(), file2.getCurrentData());
  }
}
