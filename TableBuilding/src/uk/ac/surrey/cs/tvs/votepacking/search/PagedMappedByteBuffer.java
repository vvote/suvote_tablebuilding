/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.search;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This code is derived from http://stackoverflow.com/questions/736556/binary-search-in-a-sorted-memory-mapped-file-in-java
 * 
 * It provides a Paged Memory Mapped Byte Buffer to the sorted table. We can then perform a binary search on the data to find
 * matches and return the relevant preference data.
 * 
 * The data inside needs to be sorted and all the same length
 * 
 * @author Chris Culnane
 * 
 */
public class PagedMappedByteBuffer {

  /**
   * Logger
   */
  private static final Logger    logger  = LoggerFactory.getLogger(PagedMappedByteBuffer.class);

  /**
   * Length of the line - this is the combined data and preference
   */
  private int                    lineLength;

  /**
   * The maximum size of a page - this is just in case the overall size is greater than the max value of integers
   */
  private long                   pageSize;

  /**
   * List of MappedByteBuffers in page order
   */
  private List<MappedByteBuffer> buffers = new ArrayList<MappedByteBuffer>();

  /**
   * Constructs a new PagedMappedByteBuffer from the FileChannel with a lineLength as specified
   * 
   * @param channel
   *          FileChannel of the data file
   * @param lineLength
   *          int of the line length of the data inside the file
   * @throws IOException
   */
  public PagedMappedByteBuffer(FileChannel channel, int lineLength) throws IOException {
    super();

    // Set the line length
    this.lineLength = lineLength;

    // Calculate the number of pages we will need. Each page is an exact multiple of the line length up to Integer.MAX_VALUE bytes.
    this.pageSize = (Integer.MAX_VALUE / this.lineLength) * this.lineLength;
    logger.info("Creating new PagedMappedByteBuffer with lineLength: {}", lineLength);

    // Prepare the pages
    long start = 0;
    long length = 0;

    for (long index = 0; start + length < channel.size(); index++) {
      if ((channel.size() / this.pageSize) == index) {
        length = (channel.size() - (index * this.pageSize));
      }
      else {
        length = this.pageSize;
      }
      start = index * this.pageSize;

      // Add a new MappedByteBuffer in ReadOnly mode across the bytes as specified
      this.buffers.add((int) index, channel.map(MapMode.READ_ONLY, start, length));
    }
  }

  /**
   * Gets a particular line, by byte position in respect to the whole file. The byte position should be the start of a line
   * 
   * @param bytePosition
   *          long of the start of the line - this across the whole file
   * @return byte array of line
   */
  public byte[] getLine(long bytePosition) {
    // Calculate which page it is on
    int page = (int) (bytePosition / this.pageSize);

    // Find the index within that page and the next line boundary
    int index = (int) (((bytePosition % this.pageSize) / this.lineLength) * this.lineLength);
    // Prepare a buffer
    byte[] tbuf = new byte[this.lineLength];

    // Moves to the position
    this.buffers.get(page).position(index);

    // Read the data
    this.buffers.get(page).get(tbuf);

    return tbuf;
  }

  /**
   * @return the line length
   */
  public int getLineLength() {
    return this.lineLength;
  }
}
