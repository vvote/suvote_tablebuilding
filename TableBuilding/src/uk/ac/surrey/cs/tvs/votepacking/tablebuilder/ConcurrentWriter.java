/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * ConcurrentWriter provides a way of maintaining read and write order across multiple threads performing the work. We need to write
 * the data out in the order it was read in to ensure that we can efficiently build the table. This is simple in a single threaded
 * environment, but far more complicated to achieve in a multi-threaded environment without introducing huge overhead and delays.
 * 
 * The ConcurrentWriter works by being given a reference to the Future object of the ExecutorService. It will therefore only write
 * out the data once it is processed, with the remaining data waiting in the queue until preceding data has been written. By passing
 * the Future and not the actual data we can maintain the order without significant contention.
 * 
 * The speed of the initial data being read can also be throttled by setting the size of the writeQueue. It is a blocking queue and
 * as such will block once it has reach capacity. This prevents us running out of memory by reading data faster than we can process
 * it.
 * 
 * @author Chris Culnane
 * 
 */
public class ConcurrentWriter implements Runnable {

  /**
   * Logger
   */
  private static final Logger                            logger             = LoggerFactory.getLogger(ConcurrentWriter.class);

  /**
   * Default queue size
   */
  private static final int                               DEFAULT_QUEUE_SIZE = 1000;

  /**
   * LinkedBlockingQueue to hold Future objects that once completed will be written out. Set size to throttle throughput
   */
  private LinkedBlockingQueue<Future<PermutationWorker>> writeQueue         = null;

  /**
   * The BufferedOutputStream to write the data to
   */
  private BufferedOutputStream                           bos                = null;

  /**
   * Reference to future object we use as a guard against shutting down midway through a write op
   */
  private Future<PermutationWorker>                      future             = null;

  /**
   * Constructs a ConcurrentWriter with the specified BufferedOutputStream. Note the BufferedOutputStream will be closed by the
   * ConcurrentWriter once it has finished.
   * 
   * @param bos
   *          BufferedOutputStream to write data to
   */
  public ConcurrentWriter(BufferedOutputStream bos) {
    this(bos, DEFAULT_QUEUE_SIZE);
  }

  /**
   * Constructs a ConcurrentWriter with the specified BufferedOutputStream and queueSize. Note the BufferedOutputStream will be
   * closed by the ConcurrentWriter once it has finished.
   * 
   * @param bos
   *          BufferedOutputStream to write data to
   * @param queueSize
   *          int size of the queue
   */
  public ConcurrentWriter(BufferedOutputStream bos, int queueSize) {
    super();

    this.bos = bos;
    this.writeQueue = new LinkedBlockingQueue<Future<PermutationWorker>>(queueSize);

    logger.info("Created ConcurrentWriter with queue size: {}", queueSize);
  }

  /**
   * Adds a Future object to the write queue. If the write queue is full this method will block until space is available.
   * 
   * @param data
   *          Future<PermutationWorker> object that will return a permutation worker on completion
   * @throws InterruptedException
   */
  public void addToFutureWriteQueue(Future<PermutationWorker> data) throws InterruptedException {
    this.writeQueue.put(data);
  }

  /**
   * Waits for the current set of data to have been processed.
   */
  public void checkWaitLastWrite() {
    try {
      while (this.future != null) {
        logger.info("Future not null before shutdown. Will wait");
        Thread.sleep(500);
      }
    }
    catch (InterruptedException e) {
      logger.error("Interrupted whilst waiting for future to be null");
    }
  }

  /**
   * If we have finished processing we can force the write queue to immediately drain. This is slightly more efficient that taking
   * each item separately. More importantly it provides a way of blocking the calling thread until the queue is clear and the writer
   * can be interrupted. We can tell when processing is finished by when the ExecutorService terminates.
   * 
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  public void forceDrain() throws IOException, InterruptedException, ExecutionException {
    // If the queue is already empty we return
    if (this.writeQueue.isEmpty()) {
      return;
    }
    else {
      // Drain the queue into an ArrayList and loop through writing each one. Access to the LinkedBlockingQueue is thread safe
      ArrayList<Future<PermutationWorker>> drainList = new ArrayList<Future<PermutationWorker>>();
      this.writeQueue.drainTo(drainList);

      for (Future<PermutationWorker> fpw : drainList) {
        this.bos.write(fpw.get().getData());
        this.bos.write(fpw.get().getAllButLast());
        this.bos.write(fpw.get().getLast());
      }
    }
  }

  /**
   * Runnable entry point.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      // We will run until we are interrupted
      while (true) {
        // Block until we have some data to process
        this.future = this.writeQueue.take();
        PermutationWorker pw = this.future.get();

        // Output the data to the buffered output
        this.bos.write(pw.getData());
        this.bos.write(pw.getAllButLast());
        this.bos.write(pw.getLast());
        this.future = null;
      }
    }
    catch (InterruptedException e) {
      // Interrupt is called when we have written all the data.
      if (this.writeQueue.isEmpty() && this.future == null) {
        logger.info("Interrupted ConcurrentWriter to shutdown. Queue is empty, normal shutdown");
      }
      else {
        logger.error("Interrupted ConcurrentWriter with non empty queue or non-null Future", e);
      }
    }
    catch (ExecutionException e) {
      logger.error("Exception whilst executing in ConcurrentWriter", e);
    }
    catch (IOException e) {
      logger.error("IOException in ConcurrentWriter", e);
    }
    finally {
      try {
        logger.info("Closing outputstream in ConcurrentWriter");
        this.bos.close();
        logger.info("Closed outputstream in ConcurrentWriter");
      }
      catch (IOException e) {
        logger.error("Exception whilst closing outputstream in ConcurrentWriter");
      }
    }
  }
}
