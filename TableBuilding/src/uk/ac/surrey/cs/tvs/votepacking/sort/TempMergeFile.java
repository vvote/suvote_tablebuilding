/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Holds a reference to the temp file created during the MergeSort. The contents of this file are the sub-sort produced by the
 * MergeSort. We then read one line at a time to merge the files back into a single sorted file.
 * 
 * @author Chris Culnane
 * 
 */
public class TempMergeFile {

  /**
   * Reference to the underlying file
   */
  private File                tempFile;

  /**
   * The DataInputStream to read the data from the file
   */
  private DataInputStream     dis;

  /**
   * byte array of the current line of data we have read and are waiting to merge
   */
  private byte[]              currentData = null;

  /**
   * Logger
   */
  private static final Logger logger      = LoggerFactory.getLogger(TempMergeFile.class);

  /**
   * Construct a new TempMergeFile using the specified file and lineLength
   * 
   * @param tempFile
   *          underlying File to use
   * @param lineLength
   *          number of bytes in a line
   * @throws IOException
   */
  public TempMergeFile(File tempFile, int lineLength) throws IOException {
    super();

    logger.info("Constructing new TempMergeFile at {}, with line length of {}", tempFile.getAbsolutePath(), lineLength);
    this.tempFile = tempFile;

    // Create a new data input stream
    this.dis = new DataInputStream(new BufferedInputStream(new FileInputStream(tempFile)));

    // Initialise a buffer for reading lines
    this.currentData = new byte[lineLength];

    // Read the first line
    try {
      this.dis.readFully(this.currentData);
    }
    catch (EOFException e) {
      // We have reached the end of the file (a bit unusual since this our first read)
      this.currentData = null;

      // Close and delete the temp file
      try{
        this.dis.close();
      }finally{
        IOUtils.checkAndDeleteFile(tempFile);
      }
    }
  }

  /**
   * Gets the current byte array of the data
   * 
   * @return byte array of current line or null if end of file
   */
  public byte[] getCurrentData() {
    return this.currentData;
  }

  /**
   * This gets the next chunk of data and caches it. It should be called having consumed the previous getCurrentData
   * 
   * @return byte array of next line or null if end of file
   * @throws IOException
   */
  public byte[] useCurrentData() throws IOException {
    if (this.currentData == null) {
      return null;
    }
    else {
      try {
        // Read the next line
        this.dis.readFully(this.currentData);
      }
      catch (EOFException e) {
        // We have reached the end of the file
        this.currentData = null;

        // Close and delete the temp file
        try{
          this.dis.close();
        }finally{
          IOUtils.checkAndDeleteFile(tempFile);
        }
      }

      return this.currentData;
    }
  }
}
