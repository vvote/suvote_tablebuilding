/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.verify;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.votepacking.search.BinarySearchFile;
import uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuilder;

/**
 * Provides utility methods for sampling the built table to check that the results are correct.
 * 
 * @author Chris Culnane
 * 
 */
public class VerifyBuiltTable {

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(VerifyBuiltTable.class);

  /**
   * Decrypt the cipher text.
   * 
   * @param cipher
   *          The cipher text to decrypt.
   * @param privateKey
   *          The private key used for decryption.
   * @return The plain text.
   */
  public static ECPoint decrypt(ECPoint[] cipher, BigInteger privateKey) {
    return cipher[1].add(cipher[0].multiply(privateKey).negate());
  }

  /**
   * Main entry point for verification.
   * 
   * @param args
   *          Command line arguments.
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    if (args.length < 4 || args.length > 5) {
      System.out.println("Usage: ");
      System.out.println("\t VerifyBuiltTable pathToTable lineLength blockSize numberOfTests");
      System.out.println("\t VerifyBuiltTable pathToTable candidateIDFilePath lineLength blockSize numberOfTests");
    }
    else if (args.length == 4) {
      if (!VerifyBuiltTable.testBuildTableAgainstRandomPermutations(args[0], TableBuilder.DEFAULT_CANDIDATE_FILE,
          Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]))) {
        throw new Exception("Test failed during random permutation test");
      }
      if (!VerifyBuiltTable.testBuildTableAgainstRandomEncryptedPermutations(args[0], TableBuilder.DEFAULT_CANDIDATE_FILE,
          Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3]))) {
        throw new Exception("Test failed during random encrypted permutation test");
      }
    }
    else if (args.length == 5) {
      if (!VerifyBuiltTable.testBuildTableAgainstRandomPermutations(args[0], args[1], Integer.parseInt(args[2]),
          Integer.parseInt(args[3]), Integer.parseInt(args[4]))) {
        throw new Exception("Test failed during random permutation test");
      }
      if (!VerifyBuiltTable.testBuildTableAgainstRandomEncryptedPermutations(args[0], args[1], Integer.parseInt(args[2]),
          Integer.parseInt(args[3]), Integer.parseInt(args[4]))) {
        throw new Exception("Test failed during random encrypted permutation test");
      }
    }
  }

  /**
   * Similar to testBuildTableAgainstRandomPermutations except a sample key pair are generated and the candidate ids are encrypted
   * first. The random permutations are selected in the same way and are applied in the encrypted domain to the candidates. The
   * final value is then decrypted and the output from the decryption is looked up in the table. This mimics the operations that
   * would take place on the MBB and in the mix-net. Records the min, max and average search times as we as any failures to find the
   * correct result.
   * 
   * @param tablePath
   *          String path, including folder, to the final table
   * @param candidateFile
   *          String path to candidate ids
   * @param lineLength
   *          int line length of built table
   * @param blockSize
   *          int block size to use when generating samples
   * @param numberOfTests
   *          int number of tests to perform
   * @return true if all tests pass, false otherwise.
   * @throws IOException
   * @throws JSONException
   * @throws JSONIOException
   */
  public static boolean testBuildTableAgainstRandomEncryptedPermutations(String tablePath, String candidateFile, int lineLength,
      int blockSize, int numberOfTests) throws IOException, JSONException, JSONIOException {
    logger.info("About to start test of {} random permutations", numberOfTests);
    BinarySearchFile bsf = new BinarySearchFile(new File(tablePath), lineLength);
    logger.info("Finished loading BinarySearchFile");

    // Generate a sample key pair
    logger.info("Generating sample key pair");
    ECUtils ecUtils = new ECUtils();
    BigInteger privateKey = ecUtils.getRandomInteger(ecUtils.getOrderUpperBound(), new SecureRandom());
    ECPoint publicKey = ecUtils.getG().multiply(privateKey);
    logger.info("Finished generating sample key pair");

    // Encrypt the candidate IDS
    logger.info("Starting Encryption of CandidateIDs");
    ECPoint[] candIDS = TableBuilder.readCandidateIDsFromFile(candidateFile);
    ECPoint[][] encryptedCandIDS = new ECPoint[candIDS.length][2];
    for (int i = 0; i < candIDS.length; i++) {
      encryptedCandIDS[i] = ecUtils.encrypt(candIDS[i], publicKey);
    }
    logger.info("Finished Encryption of CandidateIDs");

    ArrayList<ECPoint[]> samplePacking = new ArrayList<ECPoint[]>();
    ArrayList<int[]> expectedResult = new ArrayList<int[]>();
    SecureRandom rand = new SecureRandom();

    for (int i = 0; i < numberOfTests; i++) {
      // Select a random number of preferences to submit
      int numPrefs = rand.nextInt(blockSize) + 1;

      // Create an array to check which candidates we have picked - make sure we don't try and submit the same candidate in one set
      // of preferences
      boolean[] selectedCandidates = new boolean[candIDS.length];
      int[] expectedOutcome = new int[blockSize];
      ECPoint[] result = new ECPoint[2];

      // Iterate through number of random preferences to generate
      for (int pref = 0; pref < numPrefs; pref++) {
        // Pick a candidate at random that has not been picked in this set
        int nextCand = rand.nextInt(candIDS.length);

        while (selectedCandidates[nextCand] == true) {
          nextCand = rand.nextInt(candIDS.length);
        }
        selectedCandidates[nextCand] = true;

        // Pack the vote
        if (pref == 0) {
          result[0] = encryptedCandIDS[nextCand][0].multiply(BigInteger.valueOf(pref + 1));
          result[1] = encryptedCandIDS[nextCand][1].multiply(BigInteger.valueOf(pref + 1));
        }
        else {
          result[0] = result[0].add(encryptedCandIDS[nextCand][0].multiply(BigInteger.valueOf(pref + 1)));
          result[1] = result[1].add(encryptedCandIDS[nextCand][1].multiply(BigInteger.valueOf(pref + 1)));
        }
        expectedOutcome[pref] = nextCand + 1;
      }

      // Store the sample packing and expected outcome
      samplePacking.add(result);
      expectedResult.add(expectedOutcome);
    }
    logger.info("Finished building test permutations");

    // Decrypt the packed votes
    logger.info("Starting decryption of test permutations");
    ArrayList<ECPoint> decryptedPacking = new ArrayList<ECPoint>();

    for (int i = 0; i < numberOfTests; i++) {
      ECPoint[] result = samplePacking.get(i);
      decryptedPacking.add(VerifyBuiltTable.decrypt(result, privateKey));
    }
    logger.info("Finished decryption of test permutations");

    // Search for decrypted packed vote and check the result is correct
    logger.info("Starting search test");
    int success = 0;
    int failure = 0;
    long maxSearchTime = 0;
    long minSearchTime = Long.MAX_VALUE;

    long startTime = System.currentTimeMillis();

    for (int i = 0; i < numberOfTests; i++) {
      long innerStartTime = System.currentTimeMillis();
      byte[] res = bsf.binarySearch(decryptedPacking.get(i).getEncoded(true));
      if (res != null) {
        String foundResult = Arrays.toString(BinarySearchFile.convertToPlain(res, blockSize));
        String expected = Arrays.toString(expectedResult.get(i));

        if (!foundResult.equals(expected)) {
          logger.warn("Incorrect result); expected: {} found {}", expected, foundResult);
          failure++;
        }
        else {
          success++;
        }
      }
      else {
        failure++;
        logger.warn("Result not found: expected: {} found null", Arrays.toString(expectedResult.get(i)));
      }
      long innerEndTime = System.currentTimeMillis();
      long diff = innerEndTime - innerStartTime;

      if (diff > maxSearchTime) {
        maxSearchTime = diff;
      }
      if (diff < minSearchTime) {
        minSearchTime = diff;
      }
    }

    long endTime = System.currentTimeMillis();
    logger.info("Finished search test");
    logger.info("Test Results: Success:{}, Failures:{}", success, failure);
    logger.info("Average Search Time: {}ms", ((float) (endTime - startTime) / (float) numberOfTests));
    logger.info("Max Search Time: {}ms, Min Search Time {}", maxSearchTime, minSearchTime);

    return failure == 0;
  }

  /**
   * Creates random permutations preferences for random candidates and searches for the result in the table. Records the min, max
   * and average search times as we as any failures to find the correct result.
   * 
   * @param tablePath
   *          String path, including folder, to the final table
   * @param candidateFile
   *          String path to candidate ids
   * @param lineLength
   *          int line length of built table
   * @param blockSize
   *          int block size to use when generating samples
   * @param numberOfTests
   *          int number of tests to perform
   * @return true if all tests pass, false otherwise.
   * @throws IOException
   * @throws JSONException
   * @throws JSONIOException
   */
  public static boolean testBuildTableAgainstRandomPermutations(String tablePath, String candidateFile, int lineLength,
      int blockSize, int numberOfTests) throws IOException, JSONException, JSONIOException {
    logger.info("About to start test of {} random permutations", numberOfTests);
    BinarySearchFile bsf = new BinarySearchFile(new File(tablePath), lineLength);
    logger.info("Finished loading BinarySearchFile");

    ECPoint[] candIDS = TableBuilder.readCandidateIDsFromFile(candidateFile);
    ArrayList<ECPoint> samplePacking = new ArrayList<ECPoint>();
    ArrayList<int[]> expectedResult = new ArrayList<int[]>();
    SecureRandom rand = new SecureRandom();

    for (int i = 0; i < numberOfTests; i++) {
      // Select a random number of preferences to submit
      int numPrefs = rand.nextInt(blockSize) + 1;

      // Create an array to check which candidates we have picked - make sure we don't try and submit the same candidate in one set
      // of preferences
      boolean[] selectedCandidates = new boolean[candIDS.length];
      int[] expectedOutcome = new int[blockSize];
      ECPoint result = null;

      // Iterate through number of random preferences to generate
      for (int pref = 0; pref < numPrefs; pref++) {
        // Pick a candidate at random that has not been picked in this set
        int nextCand = rand.nextInt(candIDS.length);

        while (selectedCandidates[nextCand] == true) {
          nextCand = rand.nextInt(candIDS.length);
        }
        selectedCandidates[nextCand] = true;

        // Pack the vote
        if (pref == 0) {
          result = candIDS[nextCand].multiply(BigInteger.valueOf(pref + 1));
        }
        else {
          result = result.add(candIDS[nextCand].multiply(BigInteger.valueOf(pref + 1)));
        }

        // Set the expected outcome
        expectedOutcome[pref] = nextCand + 1;
      }

      // Store the sample packing and expected outcome
      samplePacking.add(result);
      expectedResult.add(expectedOutcome);
    }

    logger.info("Finished building test permutations");
    logger.info("Starting search test");
    int success = 0;
    int failure = 0;
    long maxSearchTime = 0;
    long minSearchTime = Long.MAX_VALUE;

    long startTime = System.currentTimeMillis();

    // Look up each sample packing and check it is found correctly
    for (int i = 0; i < numberOfTests; i++) {
      long innerStartTime = System.currentTimeMillis();
      byte[] res = bsf.binarySearch(samplePacking.get(i).getEncoded(true));
      if (res != null) {
        String foundResult = Arrays.toString(BinarySearchFile.convertToPlain(res, blockSize));
        String expected = Arrays.toString(expectedResult.get(i));

        if (!foundResult.equals(expected)) {
          logger.warn("Incorrect result); expected: {} found {}", expected, foundResult);
          failure++;
        }
        else {
          success++;
        }
      }
      else {
        failure++;
        logger.warn("Result not found: expected: {} found null", Arrays.toString(expectedResult.get(i)));
      }
      long innerEndTime = System.currentTimeMillis();
      long diff = innerEndTime - innerStartTime;

      if (diff > maxSearchTime) {
        maxSearchTime = diff;
      }
      if (diff < minSearchTime) {
        minSearchTime = diff;
      }
    }

    long endTime = System.currentTimeMillis();
    logger.info("Finished search test");
    logger.info("Test Results: Success:{}, Failures:{}", success, failure);
    logger.info("Average Search Time: {}ms", ((float) (endTime - startTime) / (float) numberOfTests));
    logger.info("Max Search Time: {}ms, Min Search Time {}", maxSearchTime, minSearchTime);

    return failure == 0;
  }

}
