/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

/**
 * Represents a cache entry from a previous generated table. It provides a convenient way of storing cache key value data.
 * 
 * @author Chris Culnane
 * 
 */
public class CacheEntry {

  /**
   * byte array to store the key
   */
  private byte[]  key;

  /**
   * byte array to store the value
   */
  private byte[]  value;

  /**
   * Placeholder to indicate this CacheEntry is the EOF marker
   */
  private boolean isEOF = false;

  /**
   * Constructs a CacheEntry that indicates an EOF
   * 
   */
  public CacheEntry() {
    super();

    this.isEOF = true;
  }

  /**
   * Construct a new CacheEntry with key and value
   * 
   * @param key
   *          byte array of key value
   * @param value
   *          byte array of value
   */
  public CacheEntry(byte[] key, byte[] value) {
    super();

    this.key = key;
    this.value = value;
  }

  /**
   * Gets the key value
   * 
   * @return byte array of key value
   */
  public byte[] getKey() {
    return this.key;
  }

  /**
   * Gets the data value
   * 
   * @return byte array of data value
   */
  public byte[] getValue() {
    return this.value;
  }

  /**
   * Checks whether this CacheEntry is the EOF marker
   * 
   * @return True if EOF, false if not
   */
  public boolean isEOF() {
    return this.isEOF;
  }
}
