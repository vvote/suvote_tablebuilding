/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Constantly fills the cache with the underlying data with a ring approach. When it reaches the end of the file it starts filling
 * the queue from the start again. As such, it is essential that shutdown is called once it is finished with.
 * 
 * @author Chris Culnane
 * 
 */
public class CacheFiller implements Runnable {

  /**
   * Reference to the underlying queue we put the CacheEntrys into
   */
  private LinkedBlockingQueue<CacheEntry> dataQueue;

  /**
   * Size of the blocks in the cache file - this is the number of permutations, allowing us to split key and value
   */
  private int                             blockSize;

  /**
   * Underlying DataInputStream we use to read the data
   */
  private DataInputStream                 dis;

  /**
   * The file to read from
   */
  private File                            dataFile;

  /**
   * shutdown flag, used to stop the filling
   */
  private boolean                         shutdown = false;

  /**
   * Logger
   */
  private static final Logger             logger   = LoggerFactory.getLogger(CacheFile.class);

  /**
   * Create a new CacheFiller and opens the input stream
   * 
   * @param dataFile
   *          File to read data from
   * @param dataQueue
   *          the underlying LinkedBlockQueue to put entries into
   * @param blockSize
   *          int block size of key value
   * @throws FileNotFoundException
   */
  public CacheFiller(File dataFile, LinkedBlockingQueue<CacheEntry> dataQueue, int blockSize) throws FileNotFoundException {
    super();

    logger.info("Creating new CacheFiller with {}", dataFile);
    this.dataQueue = dataQueue;
    this.blockSize = blockSize;
    this.dis = new DataInputStream(new BufferedInputStream(new FileInputStream(dataFile)));
    this.dataFile = dataFile;
  }

  /**
   * Runnable entry point.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      while (!this.shutdown) {
        try {
          // Create new arrays to put data into
          byte[] valueBuf = new byte[CacheFile.ENCODE_LENGTH];
          byte[] keyBuf = new byte[this.blockSize];

          // Read data from stream
          this.dis.readFully(valueBuf);
          this.dis.readFully(keyBuf);

          // Create a new CacheEntry and place in queue - will block if queue is full
          this.dataQueue.put(new CacheEntry(keyBuf, valueBuf));
        }
        catch (EOFException e) {
          // Reached the end of the file. Close it and start from the beginning again
          this.dis.close();
          this.dis = new DataInputStream(new BufferedInputStream(new FileInputStream(this.dataFile)));
        }
      }
    }
    catch (InterruptedException e) {
      logger.error("Interrupted whilst filling cache", e);
    }
    catch (IOException e) {
      logger.error("IO Exception whilst filling cache", e);
    }
    finally {
      logger.info("Closing CacheFiller stream");

      // We have shutdown gracefully or via an exception, for either close the stream
      try {
        this.dis.close();
      }
      catch (IOException e) {
        logger.error("Exception whilst closing stream", e);
      }
    }
  }

  /**
   * Sets the cacheFiller to shutdown. It sets the shutdown flag to true and empties the queue, forcing the data queue to continue
   * and thus break the loop without explicit interrupting. NOTE: This will empty the underlying cache queue.
   */
  public void shutdown() {
    logger.info("Shutting down cache filler");
    this.shutdown = true;
    this.dataQueue.clear();
  }
}
