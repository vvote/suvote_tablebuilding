/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Thread to fill the CacheData queue in the background. Unlike the more general CacheFiller this does not operate a ring paradigm.
 * Once it has reached the end of the stream it closes the stream places a null EOF CacheEntry into the queue to indicate the EOF
 * has been reached.
 * 
 * @author Chris Culnane
 * 
 */
public class CacheDataFiller implements Runnable {

  /**
   * Underlying LinkedBlockingQueue to place data into
   */
  private LinkedBlockingQueue<CacheEntry> dataQueue;

  /**
   * int subBlockSize this is the blockSize-1, where blockSize is the size of permutation being generated
   */
  private int                             subBlockSize;

  /**
   * Underlying DataInputStream to read the data from
   */
  private DataInputStream                 dis;

  /**
   * Logger
   */
  private static final Logger             logger = LoggerFactory.getLogger(CacheDataFiller.class);

  /**
   * Constructs a new CacheDataFiller to fill the CacheData queue in the background. We store the subBlockSize instead of the actual
   * block size to optimise reading of the data. We want the subBlockSize to lookup the previously calculated data in the Cache, so
   * we read it separately to the next preference value.
   * 
   * @param dis
   *          DataInputStream to read data from
   * @param dataQueue
   *          LinkedBlockingQueue to place data into
   * @param subBlockSize
   *          int subBlock size
   */
  public CacheDataFiller(DataInputStream dis, LinkedBlockingQueue<CacheEntry> dataQueue, int subBlockSize) {
    super();

    logger.info("Creating new CacheDataFiller");
    this.dataQueue = dataQueue;
    this.subBlockSize = subBlockSize;
    this.dis = dis;
  }

  /**
   * Iterates through the data in a file and places it into the dataQueue. Closes the stream when it reaches the end of file
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    try {
      try {
        // Read until the end of file
        while (true) {
          // Create two buffers, one for the subBlock and one for the next preference
          byte[] keyBuf = new byte[this.subBlockSize];
          byte[] valueBuf = new byte[1];

          // Read the values and create a cacheEntry
          this.dis.readFully(keyBuf);
          this.dis.readFully(valueBuf);
          this.dataQueue.put(new CacheEntry(keyBuf, valueBuf));
        }
      }
      catch (EOFException e) {
        logger.info("At the EOF of CacheDataFiller");

        // Puts new EOF CacheEntry to indicate end of stream
        try {
          this.dataQueue.put(new CacheEntry());
        }
        catch (InterruptedException e1) {
          logger.error("Interrupted whilst putting EOF CacheEntry in CacheDataFiller", e);
        }
      }
      catch (InterruptedException e) {
        logger.error("Interrupted whilst processing CacheDataFiller queue", e);
      }
      finally {
        logger.info("Closing DataInputStream in CacheDataFiller");
        this.dis.close();
      }
    }
    catch (IOException e) {
      logger.error("IOException in CacheDataFiller", e);
    }
  }
}
