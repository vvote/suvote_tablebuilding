Code Review 01/10/2013 MCC
==========================

No major issues with the code.  Minor comments flagged by TODO comments in the code (where you haven't done this already) and with issues against the repository. 

Minor changes have been made to each file to conform to the coding standard.  These include:
- Code tidied to ensure compliance with coding standard:
+ Methods sorted.
+ Standardised white space/blank lines.
+ Header comments completed where missing detail (for example, exceptions).
+ Method comments completed where missing detail (for example, exceptions).
+ Added "super();" call in constructors as appropriate.

General comments:
- No license.txt file in the project folder.
- license comment in header is GPL and we need to review/replace the license once a decision has been made on what license we will use.
- Empty README.md file.

Specific comments:
- BinarySearchFile.binarySearch(): foundData not set to true and break used instead.  Either remove commented out code and foundData or use foundData.
- All constructors added to DuplicateException.