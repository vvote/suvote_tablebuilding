#!/bin/sh
#Build a table with 15 candidates, packing size of 6, saved to ./output/ with a name of testTable
echo "Will log to ./logs/tablebuilding_logfile"
java -cp "./libs/*:TableBuilding.jar" -Dlogback.configurationFile=logback-build.xml -Xmx512m uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuilder 15 6 ./output/ testTable

#If you want the logging to appear on System.out remove the -Dlogback.configurationFile parameter
