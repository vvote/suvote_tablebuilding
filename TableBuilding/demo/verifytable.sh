#!/bin/sh
#Verify a table with line length of 39, packing size of 6, int ./output/testTable. Will perform 10000 tests on the table.
#The line length (39) must be the same as the line length reported by the buildtable method. By using the sample cands.txt
#this should be 39, but if the parameters are changed it will need updating. Look at the last few messages of the build log 
#to see the table size.
echo "Will log to ./logs/tableverifiy_logfile"
java -cp "./libs/*:TableBuilding.jar" -Dlogback.configurationFile=logback-verify.xml -Xmx512m uk.ac.surrey.cs.tvs.votepacking.verify.VerifyBuiltTable ./output/testTable 39 6 10000
