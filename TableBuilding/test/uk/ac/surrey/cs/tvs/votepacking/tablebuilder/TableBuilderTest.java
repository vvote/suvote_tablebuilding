/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.bouncycastle.math.ec.ECPoint;
import org.json.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.verify.VerifyBuiltTable;

/**
 * The class <code>TableBuilderTest</code> contains tests for the class <code>{@link TableBuilder}</code>.
 */
public class TableBuilderTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void createSampleCandidates(int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateSampleCandidates_1() throws Exception {
    TableBuilder.createSampleCandidates(TestParameters.CANDIDIATES);
    assertTrue(new File(TableBuilder.DEFAULT_CANDIDATE_FILE).exists());

    // Content tested elsewhere.
  }

  /**
   * Run the void createSampleCandidates(int, String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCreateSampleCandidates_2() throws Exception {
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = new File(directory, TestParameters.OUTPUT_DATA_FILE);

    TableBuilder.createSampleCandidates(TestParameters.CANDIDIATES, file.getAbsolutePath());
    assertTrue(file.exists());

    // Test the utility method for reading in the candidates.
    ECPoint[] candidates = TableBuilder.readCandidateIDsFromFile(file.getAbsolutePath());
    assertNotNull(candidates);
    assertEquals(TestParameters.CANDIDIATES, candidates.length);
    
    //CJC - Updated to reflect the change in how the candidateIDs are now stored - they are now a JSONArray instead of per line
    // Test that we have the required number of ECPoints.
    //BufferedReader reader = new BufferedReader(new FileReader(file));

    ECUtils ecUtils = new ECUtils();
    JSONArray arr = IOUtils.readJSONArrayFromFile(file.getAbsolutePath());
    //String line = reader.readLine();
    int count = 0;

    while (count<arr.length()) {
      //byte[] data = IOUtils.decodeData(EncodingType.BASE64, line);
      ECPoint read = ecUtils.pointFromJSON(arr.getJSONObject(count));
      //ECPoint read = ecUtils.getParams().getCurve().decodePoint(data);
      assertNotNull(read);
      assertEquals(read, candidates[count]);

      count++;
      //line = reader.readLine();
    }

    //reader.close();

    assertEquals(TestParameters.CANDIDIATES, count);
  }

  /**
   * Run the void doAll(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDoAll_1() throws Exception {
    // Create a temporary file which is unique so that the next test does not try to use it.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File tableFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    tableFile.deleteOnExit();

    TableBuilder fixture = new TableBuilder(TestParameters.CANDIDIATES, TestParameters.BLOCK_SIZE, directory.getAbsolutePath());
    TableBuilder.createSampleCandidates(TestParameters.CANDIDIATES);
    
    fixture.doAll(TableBuilder.DEFAULT_CANDIDATE_FILE, tableFile.getName());

    // Test that the permutations have been created.
    for (int i = 0; i < TestParameters.BLOCK_SIZE; i++) {
      File file = new File(directory, TestParameters.PERMUTATION_FILE_PREFIX + (i + 1) + TestParameters.DATA_FILE_EXT);
      assertTrue(file.exists());

      // Content tested in final output file.
    }

    // Test that the sub-table files have been created.
    for (int i = 0; i < TestParameters.BLOCK_SIZE; i++) {
      File file = new File(directory, TestParameters.SUBTABLE_FILE_PREFIX + (i + 1) + TestParameters.DATA_FILE_EXT);
      assertTrue(file.exists());

      // Content tested in final output file.
    }

    // Test that the combined data file has been created.
    assertTrue(new File(directory, TestParameters.COMBINED_FILENAME + TestParameters.DATA_FILE_EXT).exists());

    // Test that the final data file has been created with content.
    assertTrue(tableFile.exists());
    assertTrue(tableFile.length() > 0);

    // Test that the final data file has been created correctly.
    ECUtils ecUtils = new ECUtils();
    int lineLength = ecUtils.getRandomValue().getEncoded(true).length + TestParameters.BLOCK_SIZE;

    // Uses the verification code.
    assertTrue(VerifyBuiltTable.testBuildTableAgainstRandomPermutations(tableFile.getAbsolutePath(),
        TableBuilder.DEFAULT_CANDIDATE_FILE, lineLength, TestParameters.BLOCK_SIZE, 1000));
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    TableBuilder.main(new String[0]);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    // Create a temporary file which is unique so that the next test does not try to use it.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File tableFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    tableFile.deleteOnExit();

    TableBuilder.main(new String[] { Integer.toString(TestParameters.CANDIDIATES), Integer.toString(TestParameters.BLOCK_SIZE),
        TestParameters.OUTPUT_FOLDER, tableFile.getName() });

    // Test that the final data file has been created with content.
    assertTrue(tableFile.exists());
    assertTrue(tableFile.length() > 0);

    // Content tested elsewhere.
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    // Create a temporary file which is unique so that the next test does not try to use it.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File tableFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    tableFile.deleteOnExit();

    TableBuilder.main(new String[] { Integer.toString(TestParameters.CANDIDIATES), Integer.toString(TestParameters.BLOCK_SIZE),
        TestParameters.OUTPUT_FOLDER, TableBuilder.DEFAULT_CANDIDATE_FILE, tableFile.getName() });

    // Test that the final data file has been created with content.
    assertTrue(tableFile.exists());
    assertTrue(tableFile.length() > 0);

    // Content tested elsewhere.
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_4() throws Exception {
    // Create a temporary file which is unique so that the next test does not try to use it.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File tableFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    tableFile.deleteOnExit();

    // Create the candidate file.
    TableBuilder.createSampleCandidates(TestParameters.CANDIDIATES, TableBuilder.DEFAULT_CANDIDATE_FILE);
    TableBuilder.main(new String[] { Integer.toString(TestParameters.CANDIDIATES), Integer.toString(TestParameters.BLOCK_SIZE),
        TestParameters.OUTPUT_FOLDER, TableBuilder.DEFAULT_CANDIDATE_FILE, tableFile.getName() });

    // Test that the final data file has been created with content.
    assertTrue(tableFile.exists());
    assertTrue(tableFile.length() > 0);

    // Content tested elsewhere.
  }

  /**
   * Run the TableBuilder(int,int,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuilder_1() throws Exception {
    File directory = new File(TestParameters.OUTPUT_FOLDER);

    // Test folder does not exist.
    TableBuilder result = new TableBuilder(TestParameters.CANDIDIATES, TestParameters.BLOCK_SIZE, directory.getAbsolutePath());
    assertNotNull(result);

    assertTrue(directory.exists());
  }

  /**
   * Run the TableBuilder(int,int,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuilder_2() throws Exception {
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();

    // Test folder exists.
    TableBuilder result = new TableBuilder(TestParameters.CANDIDIATES, TestParameters.BLOCK_SIZE, directory.getAbsolutePath());
    assertNotNull(result);

    assertTrue(directory.exists());
  }
}