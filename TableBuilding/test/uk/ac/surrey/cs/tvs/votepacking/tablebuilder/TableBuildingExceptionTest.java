/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>TableBuildingExceptionTest</code> contains tests for the class <code>{@link TableBuildingException}</code>.
 */
public class TableBuildingExceptionTest {

  /**
   * Run the TableBuildingException() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuildingException_1() throws Exception {

    TableBuildingException result = new TableBuildingException();

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuildingException", result.toString());
    assertEquals(null, result.getMessage());
    assertEquals(null, result.getLocalizedMessage());
  }

  /**
   * Run the TableBuildingException(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuildingException_2() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;

    TableBuildingException result = new TableBuildingException(message);

    assertNotNull(result);
    assertEquals(null, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuildingException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the TableBuildingException(Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuildingException_3() throws Exception {
    Throwable cause = new Throwable();

    TableBuildingException result = new TableBuildingException(cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuildingException: java.lang.Throwable", result.toString());
    assertEquals("java.lang.Throwable", result.getMessage());
    assertEquals("java.lang.Throwable", result.getLocalizedMessage());
  }

  /**
   * Run the TableBuildingException(String,Throwable) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuildingException_4() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();

    TableBuildingException result = new TableBuildingException(message, cause);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuildingException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }

  /**
   * Run the TableBuildingException(String,Throwable,boolean,boolean) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTableBuildingException_5() throws Exception {
    String message = TestParameters.EXCEPTION_MESSAGE;
    Throwable cause = new Throwable();
    boolean enableSuppression = true;
    boolean writableStackTrace = true;

    TableBuildingException result = new TableBuildingException(message, cause, enableSuppression, writableStackTrace);

    assertNotNull(result);
    assertEquals(cause, result.getCause());
    assertEquals("uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuildingException: " + TestParameters.EXCEPTION_MESSAGE,
        result.toString());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getMessage());
    assertEquals(TestParameters.EXCEPTION_MESSAGE, result.getLocalizedMessage());
  }
}