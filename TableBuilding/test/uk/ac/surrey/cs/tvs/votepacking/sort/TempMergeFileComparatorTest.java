/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;

/**
 * The class <code>TempMergeFileComparatorTest</code> contains tests for the class <code>{@link TempMergeFileComparator}</code>.
 */
public class TempMergeFileComparatorTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the TempMergeFileComparator() constructor test.
   */
  @Test
  public void testTempMergeFileComparator_1() throws Exception {
    // Create two identical temp files.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();

    File file1 = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file1.deleteOnExit();
    TestParameters.createTestDataFile(file1, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);
    TempMergeFile tempFile1 = new TempMergeFile(file1, TestParameters.LARGE_LINE_LENGTH);

    File file2 = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file2.deleteOnExit();
    TestParameters.createTestDataFile(file2, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);
    TempMergeFile tempFile2 = new TempMergeFile(file2, TestParameters.LARGE_LINE_LENGTH);

    // Test the comparator.
    TempMergeFileComparator fixture = new TempMergeFileComparator();

    // Test the same lines.
    assertEquals(0, fixture.compare(tempFile1, tempFile2));

    // Test file 1 having a higher value by moving it on 1 line.
    tempFile1.useCurrentData();
    assertTrue(fixture.compare(tempFile1, tempFile2) > 0);

    // Test file 2 having a higher value by moving it on 2 lines.
    tempFile2.useCurrentData();
    tempFile2.useCurrentData();
    assertTrue(fixture.compare(tempFile1, tempFile2) < 0);
  }
}