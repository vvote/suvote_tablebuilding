/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>CacheFileTest</code> contains tests for the class <code>{@link CacheFile}</code>.
 */
public class CacheFileTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CacheFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = FileNotFoundException.class)
  public void testCacheFile_1() throws Exception {
    // Create a test data file.
    File file = new File("rubbish");

    // Test missing file.
    CacheFile result = new CacheFile(file, TestParameters.SUB_BLOCK_SIZE);
    assertNull(result);
  }

  /**
   * Run the CacheFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCacheFile_2() throws Exception {
    // Create a test data file.
    File file = new File(TestParameters.OUTPUT_DATA_FILE);

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);

    // Test creating the cache and starting the thread.
    CacheFile result = new CacheFile(file, TestParameters.SUB_BLOCK_SIZE);
    //TODO CJC: Following review have moved start to explicit call from constructor. Have to add start() method or an exception is thrown
    result.start();
    assertNotNull(result);

    // Create the search key.
    byte[] key = new byte[TestParameters.SUB_BLOCK_DATA.length];
    System.arraycopy(TestParameters.SUB_BLOCK_DATA, 0, key, 0, TestParameters.SUB_BLOCK_DATA.length);

    // Test finding an entry at the end of the file.
    key[0] = TestParameters.QUEUE_SIZE - 1;

    byte[] response = result.getValue(key);
    assertNotNull(response);
    assertEquals(key[0], response[0]);
    assertEquals(CacheFile.ENCODE_LENGTH, response.length);

    for (byte i = 1; i < response.length; i++) {
      assertEquals(i, response[i]);
    }

    // Test finding another entry which causes a loop around.
    key[0] = 0;

    response = result.getValue(key);
    assertNotNull(response);
    assertEquals(key[0], response[0]);
    assertEquals(CacheFile.ENCODE_LENGTH, response.length);

    for (byte i = 1; i < response.length; i++) {
      assertEquals(i, response[i]);
    }

    // Test closing.
    result.close();
  }
}