/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;

/**
 * The class <code>ByteMergeSortTest</code> contains tests for the class <code>{@link ByteMergeSort}</code>.
 */
public class ByteMergeSortTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the ByteMergeSort() constructor test.
   */
  @Test
  public void testByteMergeSort_1() throws Exception {
    ByteMergeSort result = new ByteMergeSort();
    assertNotNull(result);
  }

  /**
   * Run the void sort(File,File,int,int) method test.
   * 
   * @throws Exception
   */
  @Test(expected = FileNotFoundException.class)
  public void testSort_1() throws Exception {
    // Create a test data input file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File inFile = new File(directory, TestParameters.OUTPUT_DATA_FILE);
    File outFile = new File(directory, TestParameters.OUTPUT_DATA_FILE);

    // Test missing file.
    ByteMergeSort.sort(inFile, outFile, CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, TestParameters.SUB_BLOCK_SIZE);
  }

  /**
   * Run the void sort(File,File,int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSort_2() throws Exception {
    // Create a test data input file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File inFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    inFile.deleteOnExit();

    TestParameters.createTestDataFile(inFile, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE, 0, true, true);

    File outFile = new File(directory, TestParameters.OUTPUT_DATA_FILE);
    inFile.deleteOnExit();

    // Test an empty data file.
    ByteMergeSort.sort(inFile, outFile, CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, TestParameters.SUB_BLOCK_SIZE);

    // Test that the output data file was created and is empty as well.
    assertTrue(outFile.exists());
    assertEquals(0, outFile.length());
  }

  /**
   * Run the void sort(File,File,int,int) method test.
   * 
   * @throws Exception
   */
  @Test(expected = DuplicateException.class)
  public void testSort_3() throws Exception {
    // Create a test data input file with duplicate data.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File inFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    inFile.deleteOnExit();

    TestParameters.createTestDataFile(inFile, false, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE,
        TestParameters.QUEUE_SIZE, false, true);

    File outFile = new File(directory, TestParameters.OUTPUT_DATA_FILE);
    inFile.deleteOnExit();

    // Test an empty data file.
    ByteMergeSort.sort(inFile, outFile, CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, TestParameters.SUB_BLOCK_SIZE);

    // Test that the output data file was created and is empty as well.
    assertTrue(outFile.exists());
    assertEquals(0, outFile.length());
  }

  /**
   * Run the void sort(File,File,int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSort_4() throws Exception {
    // Create a test data input file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File inFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    inFile.deleteOnExit();

    TestParameters.createTestDataFile(inFile, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE,
        TestParameters.QUEUE_SIZE, true, true);

    File outFile = new File(directory, TestParameters.OUTPUT_DATA_FILE);
    inFile.deleteOnExit();

    // Test sorting a small file which only uses one sub-file.
    ByteMergeSort.sort(inFile, outFile, CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, TestParameters.SUB_BLOCK_SIZE);

    // Test that the output data file was created and is sorted.
    assertTrue(outFile.exists());

    // Create a file which has sorted content.
    File expectedFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX,
        directory);
    expectedFile.deleteOnExit();

    TestParameters.createTestDataFile(expectedFile, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE,
        TestParameters.QUEUE_SIZE, true, false);

    // Test the expected and sorted files match.
    assertTrue(TestParameters.compareFile(expectedFile, outFile));
  }

  /**
   * Run the void sort(File,File,int,int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSort_5() throws Exception {
    // Create a test data input file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File inFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    inFile.deleteOnExit();

    TestParameters.createTestDataFile(inFile, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE,
        TestParameters.LARGE_QUEUE_SIZE, true, true);

    File outFile = new File(directory, TestParameters.OUTPUT_DATA_FILE);
    inFile.deleteOnExit();

    // Test sorting a small file which only uses one sub-file.
    ByteMergeSort.sort(inFile, outFile, CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, TestParameters.SUB_BLOCK_SIZE);

    // Test that the output data file was created and is sorted.
    assertTrue(outFile.exists());
    assertEquals(inFile.length(), outFile.length());

    // Read in every line and check that the lines are sorted.
    ByteArrayComparator comparator = new ByteArrayComparator();
    DataInputStream dis = null;
    byte[] lastLine = null;
    byte[] line = null;

    try {
      dis = new DataInputStream(new BufferedInputStream(new FileInputStream(outFile)));

      // Read until EOF.
      while (true) {
        line = new byte[CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE];
        dis.readFully(line);

        if ((line != null) && (lastLine != null)) {
          assertTrue(comparator.compare(lastLine, line) < 0);
        }

        lastLine = line;
      }
    }
    catch (EOFException e) {
      // Successful completion.
    }
    finally {
      if (dis != null) {
        dis.close();
      }
    }
  }
}