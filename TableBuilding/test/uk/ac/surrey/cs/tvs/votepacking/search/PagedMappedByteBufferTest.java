/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;

/**
 * The class <code>PagedMappedByteBufferTest</code> contains tests for the class <code>{@link PagedMappedByteBuffer}</code>.
 */
public class PagedMappedByteBufferTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the byte[] getLine(long) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetLine_1() throws Exception {
    // Create a large test data file: more than one page (about 2GB).
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE,
        TestParameters.LARGE_QUEUE_SIZE);
    FileInputStream in = new FileInputStream(file);

    PagedMappedByteBuffer fixture = new PagedMappedByteBuffer(in.getChannel(), TestParameters.LARGE_LINE_LENGTH);

    // Test retrieving a line from the first page.
    int index = 0;
    byte[] result = fixture.getLine(index * TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(result);
    assertEquals(TestParameters.LARGE_LINE_LENGTH, result.length);

    assertEquals((byte) (Integer.parseInt(String.format("%08x", index).substring(6, 8), 16) & 0xFF), result[0]);

    for (int i = 1; i < CacheFile.ENCODE_LENGTH; i++) {
      assertEquals(i, result[i]);
    }

    assertEquals((byte) (Integer.parseInt(String.format("%08x", index).substring(6, 8), 16) & 0xFF),
        result[CacheFile.ENCODE_LENGTH]);

    for (int i = 1; i < TestParameters.SUB_BLOCK_SIZE; i++) {
      assertEquals(i, result[i]);
    }

    // Test retrieving a line from the second page.
    index = TestParameters.LARGE_QUEUE_SIZE - 1;
    result = fixture.getLine(index * TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(result);
    assertEquals(TestParameters.LARGE_LINE_LENGTH, result.length);

    assertEquals((byte) (Integer.parseInt(String.format("%08x", index).substring(6, 8), 16) & 0xFF), result[0]);

    for (int i = 1; i < CacheFile.ENCODE_LENGTH; i++) {
      assertEquals(i, result[i]);
    }

    assertEquals((byte) (Integer.parseInt(String.format("%08x", index).substring(6, 8), 16) & 0xFF),
        result[CacheFile.ENCODE_LENGTH]);

    for (int i = 1; i < TestParameters.SUB_BLOCK_SIZE; i++) {
      assertEquals(i, result[i]);
    }

    in.close();
  }

  /**
   * Run the PagedMappedByteBuffer(FileChannel,int) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = IOException.class)
  public void testPagedMappedByteBuffer_1() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);
    FileInputStream in = new FileInputStream(file);
    in.close();

    // Test object creation with invalid channel.
    PagedMappedByteBuffer result = new PagedMappedByteBuffer(in.getChannel(), TestParameters.LARGE_LINE_LENGTH);
    assertNull(result);
  }

  /**
   * Run the PagedMappedByteBuffer(FileChannel,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPagedMappedByteBuffer_2() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);
    FileInputStream in = new FileInputStream(file);

    // Test successful object creation.
    PagedMappedByteBuffer result = new PagedMappedByteBuffer(in.getChannel(), CacheFile.ENCODE_LENGTH
        + TestParameters.SUB_BLOCK_SIZE);
    assertNotNull(result);

    assertEquals(CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, result.getLineLength());

    in.close();
  }
}