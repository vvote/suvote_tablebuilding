/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>CacheEntryTest</code> contains tests for the class <code>{@link CacheEntry}</code>.
 */
public class CacheEntryTest {

  /**
   * Run the CacheEntry() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCacheEntry_1() throws Exception {
    CacheEntry result = new CacheEntry();
    assertNotNull(result);
    assertEquals(null, result.getValue());
    assertEquals(null, result.getKey());
    assertEquals(true, result.isEOF());
  }

  /**
   * Run the CacheEntry() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCacheEntry_2() throws Exception {
    CacheEntry result = new CacheEntry(TestParameters.SUB_BLOCK_DATA, new byte[] { 99 });
    assertNotNull(result);

    for (int i = 0; i < TestParameters.SUB_BLOCK_DATA.length; i++) {
      assertEquals(TestParameters.SUB_BLOCK_DATA[i], result.getKey()[i]);
    }

    assertEquals(99, result.getValue()[0]);
    assertEquals(false, result.isEOF());
  }
}