/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.verify;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.File;
import java.math.BigInteger;
import java.security.SecureRandom;

import org.bouncycastle.math.ec.ECPoint;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuilder;

/**
 * The class <code>VerifyBuiltTableTest</code> contains tests for the class <code>{@link VerifyBuiltTable}</code>.
 */
public class VerifyBuiltTableTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the ECPoint decrypt(ECPoint[],BigInteger) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testDecrypt_1() throws Exception {
    ECUtils ecUtils = new ECUtils();

    ECPoint plainText = ecUtils.getRandomValue();
    BigInteger privateKey = ecUtils.getRandomInteger(ecUtils.getOrderUpperBound(), new SecureRandom());
    ECPoint publicKey = ecUtils.getG().multiply(privateKey);

    ECPoint[] cipher = ecUtils.encrypt(plainText, publicKey);

    // Test decryption.
    ECPoint result = VerifyBuiltTable.decrypt(cipher, privateKey);
    assertNotNull(result);
    assertEquals(plainText, result);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    VerifyBuiltTable.main(new String[0]);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    // Create a temporary file which is unique so that the next test does not try to use it.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File tableFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    tableFile.deleteOnExit();

    // Create a table.
    TableBuilder.main(new String[] { Integer.toString(TestParameters.CANDIDIATES), Integer.toString(TestParameters.BLOCK_SIZE),
        TestParameters.OUTPUT_FOLDER, tableFile.getName() });

    ECUtils ecUtils = new ECUtils();
    int lineLength = ecUtils.getRandomValue().getEncoded(true).length + TestParameters.BLOCK_SIZE;

    // Test verification with the default candidate file.
    VerifyBuiltTable.main(new String[] { tableFile.getAbsolutePath(), Integer.toString(lineLength),
        Integer.toString(TestParameters.BLOCK_SIZE), Integer.toString(1000) });
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    // Create a temporary file which is unique so that the next test does not try to use it.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File tableFile = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    tableFile.deleteOnExit();

    // Create a table.
    TableBuilder.main(new String[] { Integer.toString(TestParameters.CANDIDIATES), Integer.toString(TestParameters.BLOCK_SIZE),
        TestParameters.OUTPUT_FOLDER, tableFile.getName() });

    ECUtils ecUtils = new ECUtils();
    int lineLength = ecUtils.getRandomValue().getEncoded(true).length + TestParameters.BLOCK_SIZE;

    // Test verification with the default candidate file.
    VerifyBuiltTable.main(new String[] { tableFile.getAbsolutePath(), TableBuilder.DEFAULT_CANDIDATE_FILE,
        Integer.toString(lineLength), Integer.toString(TestParameters.BLOCK_SIZE), Integer.toString(1000) });
  }

  /**
   * Run the VerifyBuiltTable() constructor test.
   */
  @Test
  public void testVerifyBuiltTable_1() throws Exception {
    VerifyBuiltTable result = new VerifyBuiltTable();
    assertNotNull(result);
  }
}