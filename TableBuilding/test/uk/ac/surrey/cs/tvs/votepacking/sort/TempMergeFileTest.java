/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;

/**
 * The class <code>TempMergeFileTest</code> contains tests for the class <code>{@link TempMergeFile}</code>.
 */
public class TempMergeFileTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the TempMergeFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTempMergeFile_1() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE, 0);

    // Successfully opening the file with no data entries.
    TempMergeFile fixture = new TempMergeFile(file, TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(fixture);

    // Test that the content was read successfully.
    byte[] result = fixture.getCurrentData();
    assertNull(result);

    // Test that the file was deleted.
    assertFalse(file.exists());
  }

  /**
   * Run the TempMergeFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTempMergeFile_2() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);

    // Successfully opening the file with more than one data entry.
    TempMergeFile fixture = new TempMergeFile(file, TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(fixture);

    // Test that the content was read successfully.
    byte[] result = fixture.getCurrentData();
    assertNotNull(result);
    assertEquals(CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, result.length);

    assertEquals(0, result[0]);

    for (int i = 1; i < CacheFile.ENCODE_LENGTH; i++) {
      assertEquals(i, result[i]);
    }

    assertEquals(0, result[CacheFile.ENCODE_LENGTH]);

    for (int i = 1; i < TestParameters.SUB_BLOCK_SIZE; i++) {
      assertEquals(i, result[i]);
    }

    // Test that the file still exists (more data to be read).
    assertTrue(file.exists());
  }

  /**
   * Run the byte[] useCurrentData() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUseCurrentData_1() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE, 0);

    // Successfully opening the file with no data entries.
    TempMergeFile fixture = new TempMergeFile(file, TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(fixture);

    // Test that no data can be read.
    byte[] result = fixture.useCurrentData();
    assertNull(result);
  }

  /**
   * Run the byte[] useCurrentData() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testUseCurrentData_2() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);

    // Successfully opening the file with data entries.
    TempMergeFile fixture = new TempMergeFile(file, TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(fixture);

    // Test that data can be read until EOF.
    int index = 1; // First line of data already read by constructor.
    byte[] result = fixture.useCurrentData();

    while (result != null) {
      assertEquals(CacheFile.ENCODE_LENGTH + TestParameters.SUB_BLOCK_SIZE, result.length);

      assertEquals((byte) (index % Byte.MAX_VALUE), result[0]);

      for (int i = 1; i < CacheFile.ENCODE_LENGTH; i++) {
        assertEquals(i, result[i]);
      }

      assertEquals((byte) (Integer.parseInt(String.format("%08x", index).substring(6, 8), 16) & 0xFF),
          result[CacheFile.ENCODE_LENGTH]);

      for (int i = 1; i < TestParameters.SUB_BLOCK_SIZE; i++) {
        assertEquals(i, result[i]);
      }

      result = fixture.useCurrentData();
      index++;
    }

    // Test that the file was deleted.
    assertFalse(file.exists());
  }
}