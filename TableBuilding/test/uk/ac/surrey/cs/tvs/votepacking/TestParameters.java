/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;
import uk.ac.surrey.cs.tvs.votepacking.tablebuilder.TableBuilder;

/**
 * This class defines common test parameters and static initialisers within a singleton.
 */
public class TestParameters {

  /** Dummy exception message used for testing. */
  public static final String    EXCEPTION_MESSAGE       = "Exception Message";

  /** Output folder for files. */
  public static final String    OUTPUT_FOLDER           = "output";

  /** Output file prefix. */
  public static final String    OUTPUT_DATA_FILE_PREFIX = "data";

  /** Output file suffix. */
  public static final String    OUTPUT_DATA_FILE_SUFFIX = ".txt";

  /** Output file for data. */
  public static final String    OUTPUT_DATA_FILE        = OUTPUT_DATA_FILE_PREFIX + OUTPUT_DATA_FILE_SUFFIX;

  /** Test data queue size. */
  public static final int       QUEUE_SIZE              = 10;

  /** Test sub-block size for cache. */
  public static final int       SUB_BLOCK_SIZE          = 16;

  /** Test large data line length. */
  public static final int       LARGE_LINE_LENGTH       = CacheFile.ENCODE_LENGTH + SUB_BLOCK_SIZE;

  /** Test large data queue size: more than one page. */
  public static final int       LARGE_QUEUE_SIZE        = (Integer.MAX_VALUE / LARGE_LINE_LENGTH) + 1;

  /** Test data matching sub-block size. */
  public static final byte[]    SUB_BLOCK_DATA          = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };

  /** Test block size for cache. */
  public static final int       BLOCK_SIZE              = 3;

  /** Test candidates. */
  public static final int       CANDIDIATES             = 6;

  /** Test block all but last data. */
  public static final byte[]    ALL_BUT_LAST_DATA       = new byte[] { 3, 2 };

  /** Test block last data. */
  public static final byte[]    LAST_DATA               = new byte[] { 1 };

  /** Extension for files generated during table building. */
  public static final String    DATA_FILE_EXT           = ".dat";

  /** Prefix for permutation files. */
  public static final String    PERMUTATION_FILE_PREFIX = "permutations_block_";

  /** Prefix for sub-table files. */
  public static final String    SUBTABLE_FILE_PREFIX    = "table_block_";

  /** Filename for combined file. */
  public static final String    COMBINED_FILENAME       = "combined";

  /** Test sleep interval. */
  public static final int       SLEEP_INTERVAL          = 100;

  /** Default buffer size to use when copying files. */
  private static final int      BUFFER_SIZE             = 8192;

  /** The singleton instance of these parameters. */
  private static TestParameters instance                = null;

  /**
   * Private default constructor to prevent instantiation.
   */
  private TestParameters() {
    super();

    // Initialise BouncyCastle.
    CryptoUtils.initProvider();
  }

  /**
   * Compares the content of two files.
   * 
   * @param file1
   *          Input file 1.
   * @param file2
   *          Input file 2.
   * @return True if the files are identical, false otherwise.
   * @throws IOException
   *           on failure to read.
   */
  public static boolean compareFile(File file1, File file2) throws IOException {
    boolean result = true;

    byte[] buffer1 = new byte[BUFFER_SIZE];
    byte[] buffer2 = new byte[BUFFER_SIZE];

    InputStream in1 = null;
    InputStream in2 = null;

    try {
      in1 = new FileInputStream(file1);
      in2 = new FileInputStream(file2);

      int numBytes1 = in1.read(buffer1);
      int numBytes2 = in2.read(buffer2);

      while ((numBytes1 != -1) && (numBytes2 != -1) && result) {
        if (numBytes1 != numBytes2) {
          result = false;
        }
        else {
          for (int i = 0; i < numBytes1; i++) {
            if (buffer1[i] != buffer2[i]) {
              result = false;
            }
          }
        }

        numBytes1 = in1.read(buffer1);
        numBytes2 = in2.read(buffer2);
      }
    }
    // Throw exceptions up.
    finally {
      if (in1 != null) {
        in1.close();
      }
      if (in2 != null) {
        in2.close();
      }
    }

    return result;
  }

  /**
   * Creates and populates a test data file with byte data of the default type.
   * 
   * @param file
   *          The file to create and populate.
   * @throws IOException
   */
  public static void createTestDataFile(File file) throws IOException {
    createTestDataFile(file, false, TestParameters.SUB_BLOCK_SIZE, 1);
  }

  /**
   * Creates and populates a test data file with byte data of the default type and size.
   * 
   * @param file
   *          The file to create and populate.
   * @param includeIndex
   *          Include the current row index in the key?
   * @param length1
   *          The length in bytes of the first data element.
   * @param length2
   *          The length in bytes of the second data element.
   * @throws IOException
   */
  public static void createTestDataFile(File file, boolean includeIndex, int length1, int length2) throws IOException {
    createTestDataFile(file, includeIndex, length1, length2, QUEUE_SIZE);
  }

  /**
   * Creates and populates a test data file with byte data.
   * 
   * @param file
   *          The file to create and populate.
   * @param includeIndex
   *          Include the current row index in the key?
   * @param length1
   *          The length in bytes of the first data element.
   * @param length2
   *          The length in bytes of the second data element.
   * @param size
   *          The number of items to write.
   * @throws IOException
   */
  public static void createTestDataFile(File file, boolean includeIndex, int length1, int length2, int size) throws IOException {
    createTestDataFile(file, includeIndex, length1, length2, size, false, false);
  }

  /**
   * Creates and populates a test data file with byte data.
   * 
   * @param file
   *          The file to create and populate.
   * @param includeIndex
   *          Include the current row index in the key?
   * @param length1
   *          The length in bytes of the first data element.
   * @param length2
   *          The length in bytes of the second data element.
   * @param size
   *          The number of items to write.
   * @param unique
   *          Make each entry unique by using each byte incrementally?
   * @param reverse
   *          Create entries in reverse order?
   * @throws IOException
   */
  public static void createTestDataFile(File file, boolean includeIndex, int length1, int length2, int size, boolean unique,
      boolean reverse) throws IOException {
    BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(file));

    // Write multiple queue entries with the index in the value.
    for (int i = 0; i < size; i++) {
      // Create the first.
      byte[] first = new byte[length1];

      for (byte j = 0; j < first.length; j++) {
        first[j] = j;
      }

      // Create the second.
      byte[] second = new byte[length2];

      for (byte j = 0; j < second.length; j++) {
        second[j] = j;
      }

      // Convert the current size index into 4 bytes, depending on the order.
      int index = i;

      if (reverse) {
        index = size - 1 - i;
      }

      String hex = String.format("%08x", index);
      byte i4 = (byte) (Integer.parseInt(hex.substring(0, 2), 16) & 0xFF);
      byte i3 = (byte) (Integer.parseInt(hex.substring(2, 4), 16) & 0xFF);
      byte i2 = (byte) (Integer.parseInt(hex.substring(4, 6), 16) & 0xFF);
      byte i1 = (byte) (Integer.parseInt(hex.substring(6, 8), 16) & 0xFF);

      // Add in some test data for searching.
      if (includeIndex) {
        first[0] = i1;
      }

      // Do we want to make the data unique? We do this by using the index in subsequent bytes.
      if (unique) {
        first[1] = i2;
        first[2] = i3;
        first[3] = i4;
      }

      // The second is always the index.
      second[0] = i1;

      // Write the values.
      bw.write(first);
      bw.write(second);
    }

    bw.close();
  }

  /**
   * Recursive method to delete a folder and its content. If something goes wrong, this method remains silent.
   * 
   * @param file
   *          The file/folder to delete.
   */
  public static void deleteRecursive(File file) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive(child);
      }
    }

    file.delete();
  }

  /**
   * Singleton access method.
   * 
   * @return The test parameters singleton instance.
   */
  public static TestParameters getInstance() {
    // Lazy creation.
    if (instance == null) {
      instance = new TestParameters();
    }

    return instance;
  }

  /**
   * Deletes all of the test output files that are generated.
   */
  public static void tidyFiles() {
    deleteRecursive(new File(OUTPUT_DATA_FILE));
    deleteRecursive(new File(OUTPUT_FOLDER));
    deleteRecursive(new File(TableBuilder.DEFAULT_CANDIDATE_FILE));
  }

  /**
   * Wait for the specified number of loops.
   * 
   * @param count
   *          The number of loops.
   * @return
   */
  public static void wait(int count) {
    for (int i = 0; i < count; i++) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }
    }
  }
}
