/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.bouncycastle.math.ec.ECPoint;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>ConcurrentWriterTest</code> contains tests for the class <code>{@link ConcurrentWriter}</code>.
 */
public class ConcurrentWriterTest {

  /**
   * Dummy Future class for testing.
   */
  private class DummyFuture implements Future<PermutationWorker> {

    private PermutationWorker worker = null;

    public DummyFuture(PermutationWorker worker) {
      super();

      this.worker = worker;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
      return false;
    }

    @Override
    public PermutationWorker get() throws InterruptedException, ExecutionException {
      return this.worker;
    }

    @Override
    public PermutationWorker get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
      return this.worker;
    }

    @Override
    public boolean isCancelled() {
      return false;
    }

    @Override
    public boolean isDone() {
      return false;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the ConcurrentWriter(BufferedOutputStream) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testConcurrentWriter_1() throws Exception {
    // Create a test output file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));

    ConcurrentWriter result = new ConcurrentWriter(bos);
    assertNotNull(result);

    bos.close();

    assertTrue(file.exists());
    assertEquals(0, file.length());
  }

  /**
   * Run the ConcurrentWriter(BufferedOutputStream,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testConcurrentWriter_2() throws Exception {
    // Create a test output file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));

    ConcurrentWriter result = new ConcurrentWriter(bos, 99);
    assertNotNull(result);

    bos.close();

    assertTrue(file.exists());
    assertEquals(0, file.length());
  }

  /**
   * Run the void forceDrain() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testForceDrain_1() throws Exception {
    // Create a test output file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));

    ConcurrentWriter fixture = new ConcurrentWriter(bos);

    // Test an empty queue.
    fixture.forceDrain();

    bos.close();

    assertTrue(file.exists());
    assertEquals(0, file.length());
  }

  /**
   * Run the void forceDrain() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testForceDrain_2() throws Exception {
    // Create a test output file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    // Create a builder specification with some dummy candidates and permutation result.
    ECUtils ecUtils = new ECUtils();

    ECPoint[][] scaledCandidateCache = new ECPoint[TestParameters.CANDIDIATES][TestParameters.BLOCK_SIZE];

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.BLOCK_SIZE; blockCounter++) {
        scaledCandidateCache[candCounter][blockCounter] = ecUtils.getRandomValue().multiply(BigInteger.valueOf(blockCounter + 1));
      }
    }

    BuilderSpec builderSpec = new BuilderSpec(scaledCandidateCache, TestParameters.BLOCK_SIZE);

    ECPoint point1 = builderSpec.ecUtils.getRandomValue();
    byte[] cache1 = point1.getEncoded();
    PermutationWorker worker1 = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA,
        cache1);
    worker1.call();
    ECPoint expectedData1 = point1.add(builderSpec.getScaledCandidate(TestParameters.LAST_DATA[0] - 1,
        TestParameters.BLOCK_SIZE - 1));

    ECPoint point2 = builderSpec.ecUtils.getRandomValue();
    byte[] cache2 = point2.getEncoded();
    PermutationWorker worker2 = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA,
        cache2);
    worker2.call();
    ECPoint expectedData2 = point2.add(builderSpec.getScaledCandidate(TestParameters.LAST_DATA[0] - 1,
        TestParameters.BLOCK_SIZE - 1));

    // Create the object.
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));

    ConcurrentWriter fixture = new ConcurrentWriter(bos);

    // Test adding items to the queue.
    fixture.addToFutureWriteQueue(new DummyFuture(worker1));
    fixture.addToFutureWriteQueue(new DummyFuture(worker2));

    // Test an empty queue.
    fixture.forceDrain();

    bos.close();

    assertTrue(file.exists());

    // Test the file content.
    BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));

    byte[] data1 = new byte[expectedData1.getEncoded().length];
    in.read(data1);
    assertEquals(expectedData1, builderSpec.ecUtils.getParams().getCurve().decodePoint(data1));

    byte[] allButLast1 = new byte[worker1.getAllButLast().length];
    in.read(allButLast1);
    for (int i = 0; i < allButLast1.length; i++) {
      assertEquals(worker1.getAllButLast()[i], allButLast1[i]);
    }

    byte[] last1 = new byte[worker1.getLast().length];
    in.read(last1);
    for (int i = 0; i < last1.length; i++) {
      assertEquals(worker1.getLast()[i], last1[i]);
    }

    byte[] data2 = new byte[expectedData2.getEncoded().length];
    in.read(data2);
    assertEquals(expectedData2, builderSpec.ecUtils.getParams().getCurve().decodePoint(data2));

    byte[] allButLast2 = new byte[worker2.getAllButLast().length];
    in.read(allButLast2);
    for (int i = 0; i < allButLast2.length; i++) {
      assertEquals(worker2.getAllButLast()[i], allButLast2[i]);
    }

    byte[] last2 = new byte[worker2.getLast().length];
    in.read(last2);
    for (int i = 0; i < last2.length; i++) {
      assertEquals(worker2.getLast()[i], last2[i]);
    }

    in.close();
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    // Create a test output file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);

    // Create a builder specification with some dummy candidates and permutation result.
    ECUtils ecUtils = new ECUtils();

    ECPoint[][] scaledCandidateCache = new ECPoint[TestParameters.CANDIDIATES][TestParameters.BLOCK_SIZE];

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.BLOCK_SIZE; blockCounter++) {
        scaledCandidateCache[candCounter][blockCounter] = ecUtils.getRandomValue().multiply(BigInteger.valueOf(blockCounter + 1));
      }
    }

    BuilderSpec builderSpec = new BuilderSpec(scaledCandidateCache, TestParameters.BLOCK_SIZE);

    ECPoint point1 = builderSpec.ecUtils.getRandomValue();
    byte[] cache1 = point1.getEncoded();
    PermutationWorker worker1 = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA,
        cache1);
    worker1.call();
    ECPoint expectedData1 = point1.add(builderSpec.getScaledCandidate(TestParameters.LAST_DATA[0] - 1,
        TestParameters.BLOCK_SIZE - 1));

    ECPoint point2 = builderSpec.ecUtils.getRandomValue();
    byte[] cache2 = point2.getEncoded();
    PermutationWorker worker2 = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA,
        cache2);
    worker2.call();
    ECPoint expectedData2 = point2.add(builderSpec.getScaledCandidate(TestParameters.LAST_DATA[0] - 1,
        TestParameters.BLOCK_SIZE - 1));

    // Create the object.
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));

    ConcurrentWriter fixture = new ConcurrentWriter(bos);

    // Create and start a thread to run the writer within.
    Thread thread = new Thread(fixture);
    thread.start();

    // Add some data to process and wait for the write to finish.
    fixture.addToFutureWriteQueue(new DummyFuture(worker1));
    fixture.checkWaitLastWrite();

    fixture.addToFutureWriteQueue(new DummyFuture(worker2));
    fixture.checkWaitLastWrite();

    // Instead of forcing a drain (already tested, wait for all of the data to have been written).
    TestParameters.wait(3);

    // Make sure the thread is finished.
    thread.interrupt();
    thread.join();
    assertFalse(thread.isAlive());

    bos.close();

    assertTrue(file.exists());

    // Test the file content.
    BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));

    byte[] data1 = new byte[expectedData1.getEncoded().length];
    in.read(data1);
    assertEquals(expectedData1, builderSpec.ecUtils.getParams().getCurve().decodePoint(data1));

    byte[] allButLast1 = new byte[worker1.getAllButLast().length];
    in.read(allButLast1);
    for (int i = 0; i < allButLast1.length; i++) {
      assertEquals(worker1.getAllButLast()[i], allButLast1[i]);
    }

    byte[] last1 = new byte[worker1.getLast().length];
    in.read(last1);
    for (int i = 0; i < last1.length; i++) {
      assertEquals(worker1.getLast()[i], last1[i]);
    }

    byte[] data2 = new byte[expectedData2.getEncoded().length];
    in.read(data2);
    assertEquals(expectedData2, builderSpec.ecUtils.getParams().getCurve().decodePoint(data2));

    byte[] allButLast2 = new byte[worker2.getAllButLast().length];
    in.read(allButLast2);
    for (int i = 0; i < allButLast2.length; i++) {
      assertEquals(worker2.getAllButLast()[i], allButLast2[i]);
    }

    byte[] last2 = new byte[worker2.getLast().length];
    in.read(last2);
    for (int i = 0; i < last2.length; i++) {
      assertEquals(worker2.getLast()[i], last2[i]);
    }

    in.close();
  }
}