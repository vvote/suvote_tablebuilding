/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>CacheDataFileTest</code> contains tests for the class <code>{@link CacheDataFile}</code>.
 */
public class CacheDataFileTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CacheDataFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = FileNotFoundException.class)
  public void testCacheDataFile_1() throws Exception {
    // Create a test data file.
    File file = new File("rubbish");

    // Test missing file.
    CacheDataFile result = new CacheDataFile(file, TestParameters.SUB_BLOCK_SIZE);
    assertNull(result);
  }

  /**
   * Run the CacheDataFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCacheDataFile_2() throws Exception {
    // Create a test data file.
    File file = new File(TestParameters.OUTPUT_DATA_FILE);

    TestParameters.createTestDataFile(file);

    // Test creating the cache and starting the thread.
    CacheDataFile result = new CacheDataFile(file, TestParameters.SUB_BLOCK_SIZE);
    assertNotNull(result);
    //TODO CJC: Following review have moved start to explicit call from constructor. Have to add start() method or an exception is thrown
    result.start();
    // Test that all the data has been read in correctly.
    CacheEntry next = result.getNextValue();
    int count = 0;

    while (next != null) {
      for (int i = 0; i < TestParameters.SUB_BLOCK_DATA.length; i++) {
        assertEquals(TestParameters.SUB_BLOCK_DATA[i], next.getKey()[i]);
      }

      assertEquals(count, next.getValue()[0]);

      count++;
      next = result.getNextValue();
    }

    assertEquals(TestParameters.QUEUE_SIZE, count);

    // Try getting another entry.
    next = result.getNextValue();
    assertNull(next);
  }
}