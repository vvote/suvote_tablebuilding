/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * The class <code>TVSPermutationGeneratorTest</code> contains tests for the class <code>{@link TVSPermutationGenerator}</code>.
 */
public class TVSPermutationGeneratorTest {

  /**
   * Run the TVSPermutationGenerator(int,int,int,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testTVSPermutationGenerator_1() throws Exception {
    // Test a simple 2 from 4 permutation.
    int[][] expected = new int[][] { new int[] { 1, 2 }, new int[] { 1, 3 }, new int[] { 1, 4 }, new int[] { 2, 1 },
        new int[] { 2, 3 }, new int[] { 2, 4 }, new int[] { 3, 1 }, new int[] { 3, 2 }, new int[] { 3, 4 }, new int[] { 4, 1 },
        new int[] { 4, 2 }, new int[] { 4, 3 } };

    TVSPermutationGenerator result = new TVSPermutationGenerator(4, 2, 1, 4);
    assertNotNull(result);

    int index = 0;

    while (result.hasMore()) {
      int[] permutations = result.getNextPermutation();
      assertEquals(expected[index].length, permutations.length);

      for (int i = 0; i < permutations.length; i++) {
        assertEquals(expected[index][i], permutations[i]);
      }

      index++;
    }

    assertEquals(expected.length, index);
  }
}