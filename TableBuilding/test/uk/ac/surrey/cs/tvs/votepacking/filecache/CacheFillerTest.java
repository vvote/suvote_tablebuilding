/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>CacheFillerTest</code> contains tests for the class <code>{@link CacheFiller}</code>.
 */
public class CacheFillerTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CacheFiller(File,LinkedBlockingQueue<CacheEntry>,int) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = FileNotFoundException.class)
  public void testCacheFiller_1() throws Exception {
    File file = new File(TestParameters.OUTPUT_DATA_FILE);
    LinkedBlockingQueue<CacheEntry> dataQueue = new LinkedBlockingQueue<CacheEntry>();

    CacheFiller result = new CacheFiller(file, dataQueue, TestParameters.SUB_BLOCK_SIZE);
    assertNull(result);
  }

  /**
   * Run the CacheFiller(File,LinkedBlockingQueue<CacheEntry>,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCacheFiller_2() throws Exception {
    // Create a test data file.
    File file = new File(TestParameters.OUTPUT_DATA_FILE);

    TestParameters.createTestDataFile(file);
    LinkedBlockingQueue<CacheEntry> dataQueue = new LinkedBlockingQueue<CacheEntry>();

    CacheFiller result = new CacheFiller(file, dataQueue, TestParameters.SUB_BLOCK_SIZE);
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    // Create a test data file.
    File file = new File(TestParameters.OUTPUT_DATA_FILE);

    TestParameters.createTestDataFile(file);
    LinkedBlockingQueue<CacheEntry> dataQueue = new LinkedBlockingQueue<CacheEntry>();

    CacheFiller fixture = new CacheFiller(file, dataQueue, TestParameters.SUB_BLOCK_SIZE);

    // Create and start a thread to run the fixture.
    Thread thread = new Thread(fixture);
    thread.start();

    // Give the thread chance to read the data multiple times.
    TestParameters.wait(1);

    // Shutdown the thread.
    fixture.shutdown();
    thread.join();

    // Every key should be sequential within a byte value.
    byte count = 0;

    for (CacheEntry cacheEntry : dataQueue) {
      for (int j = 0; j < TestParameters.SUB_BLOCK_DATA.length; j++) {
        assertEquals(TestParameters.SUB_BLOCK_DATA[j], cacheEntry.getKey()[j]);
      }

      byte value = (byte) (Integer.parseInt(String.format("%08x", count).substring(6, 8), 16) & 0xFF);
      assertEquals(value, cacheEntry.getValue()[0]);

      count++;

      if (count >= TestParameters.QUEUE_SIZE) {
        count = 0;
      }
    }
  }
}