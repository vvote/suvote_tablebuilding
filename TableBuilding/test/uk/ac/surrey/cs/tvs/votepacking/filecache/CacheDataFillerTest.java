/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.filecache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.LinkedBlockingQueue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>CacheDataFillerTest</code> contains tests for the class <code>{@link CacheDataFiller}</code>.
 */
public class CacheDataFillerTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the CacheDataFiller(DataInputStream,LinkedBlockingQueue<CacheEntry>,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCacheDataFiller_1() throws Exception {
    DataInputStream dis = new DataInputStream(new ByteArrayInputStream(new byte[0]));
    LinkedBlockingQueue<CacheEntry> dataQueue = new LinkedBlockingQueue<CacheEntry>();

    CacheDataFiller result = new CacheDataFiller(dis, dataQueue, TestParameters.SUB_BLOCK_SIZE);
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    // Create a test data file.
    File file = new File(TestParameters.OUTPUT_DATA_FILE);

    TestParameters.createTestDataFile(file);

    // Set up the fixture.
    DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));
    LinkedBlockingQueue<CacheEntry> dataQueue = new LinkedBlockingQueue<CacheEntry>();

    CacheDataFiller fixture = new CacheDataFiller(dis, dataQueue, TestParameters.SUB_BLOCK_SIZE);

    // Test reading the data into the queue.
    fixture.run();

    // Test that all the data has been read in correctly.
    assertEquals(TestParameters.QUEUE_SIZE + 1, dataQueue.size());

    for (int i = 0; i < TestParameters.QUEUE_SIZE; i++) { // Excludes EOF entry.
      CacheEntry entry = dataQueue.poll();

      for (int j = 0; j < TestParameters.SUB_BLOCK_DATA.length; j++) {
        assertEquals(TestParameters.SUB_BLOCK_DATA[j], entry.getKey()[j]);
      }

      assertEquals(i, entry.getValue()[0]);
    }

    CacheEntry entry = dataQueue.poll();
    assertTrue(entry.isEOF());
  }
}