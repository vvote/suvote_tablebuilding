/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>BuilderSpecTest</code> contains tests for the class <code>{@link BuilderSpec}</code>.
 */
public class BuilderSpecTest {

  /**
   * Run the BuilderSpec(ECPoint[][],int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBuilderSpec_1() throws Exception {
    // Create some dummy candidates in the cache.
    ECUtils ecUtils = new ECUtils();

    ECPoint[][] scaledCandidateCache = new ECPoint[TestParameters.CANDIDIATES][TestParameters.SUB_BLOCK_SIZE];

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.SUB_BLOCK_SIZE; blockCounter++) {
        scaledCandidateCache[candCounter][blockCounter] = ecUtils.getRandomValue().multiply(BigInteger.valueOf(blockCounter + 1));
      }
    }

    // Test successful object creation.
    BuilderSpec result = new BuilderSpec(scaledCandidateCache, TestParameters.SUB_BLOCK_SIZE);
    assertNotNull(result);
    assertNotNull(result.getECUtils());

    assertEquals(TestParameters.SUB_BLOCK_SIZE, result.getBlockSize());

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.SUB_BLOCK_SIZE; blockCounter++) {
        assertEquals(scaledCandidateCache[candCounter][blockCounter], result.getScaledCandidate(candCounter, blockCounter));
      }
    }
  }
}