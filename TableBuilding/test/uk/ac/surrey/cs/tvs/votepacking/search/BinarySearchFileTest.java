/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.search;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;
import uk.ac.surrey.cs.tvs.votepacking.filecache.CacheFile;

/**
 * The class <code>BinarySearchFileTest</code> contains tests for the class <code>{@link BinarySearchFile}</code>.
 */
public class BinarySearchFileTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the byte[] binarySearch(byte[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testBinarySearch_1() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);

    BinarySearchFile fixture = new BinarySearchFile(file, TestParameters.LARGE_LINE_LENGTH);

    // Test successful search.
    byte[] searchData = new byte[CacheFile.ENCODE_LENGTH];

    searchData[0] = TestParameters.QUEUE_SIZE - 2; // Somewhere towards the end.

    for (byte i = 1; i < searchData.length; i++) {
      searchData[i] = i;
    }

    byte[] result = fixture.binarySearch(searchData);
    assertNotNull(result);
    assertEquals(TestParameters.LARGE_LINE_LENGTH, result.length);

    for (int i = 0; i < CacheFile.ENCODE_LENGTH; i++) {
      assertEquals(searchData[i], result[i]);
    }

    assertEquals(searchData[0], result[CacheFile.ENCODE_LENGTH]);

    for (int i = 1; i < TestParameters.SUB_BLOCK_SIZE; i++) {
      assertEquals(i, result[i]);
    }

    // Extract the data and test it.
    result = BinarySearchFile.convertToPlain(result, TestParameters.SUB_BLOCK_SIZE);
    assertNotNull(result);

    assertEquals(searchData[0], result[0]);

    for (int i = 1; i < TestParameters.SUB_BLOCK_SIZE; i++) {
      assertEquals(i, result[i]);
    }

    fixture.close();
  }

  /**
   * Run the byte[] binarySearch(byte[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testBinarySearch_2() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);

    BinarySearchFile fixture = new BinarySearchFile(file, TestParameters.LARGE_LINE_LENGTH);

    // Test unsuccessful search.
    byte[] searchData = new byte[CacheFile.ENCODE_LENGTH];

    searchData[0] = TestParameters.QUEUE_SIZE + 2; // Somewhere beyond the end.

    for (byte i = 1; i < searchData.length; i++) {
      searchData[i] = i;
    }

    byte[] result = fixture.binarySearch(searchData);
    assertNull(result);

    fixture.close();
  }

  /**
   * Run the BinarySearchFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = FileNotFoundException.class)
  public void testBinarySearchFile_1() throws Exception {
    // Define a test data file.
    File file = new File(TestParameters.OUTPUT_DATA_FILE);

    // Test missing file.
    BinarySearchFile result = new BinarySearchFile(file, TestParameters.LARGE_LINE_LENGTH);
    assertNull(result);
  }

  /**
   * Run the BinarySearchFile(File,int) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testBinarySearchFile_2() throws Exception {
    // Create a test data file.
    File directory = new File(TestParameters.OUTPUT_FOLDER);
    directory.mkdir();
    File file = File.createTempFile(TestParameters.OUTPUT_DATA_FILE_PREFIX, TestParameters.OUTPUT_DATA_FILE_SUFFIX, directory);
    file.deleteOnExit();

    TestParameters.createTestDataFile(file, true, CacheFile.ENCODE_LENGTH, TestParameters.SUB_BLOCK_SIZE);

    // Test successful object creation.
    BinarySearchFile result = new BinarySearchFile(file, TestParameters.LARGE_LINE_LENGTH);
    assertNotNull(result);

    result.close();
  }
}