/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.tablebuilder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.math.BigInteger;

import org.bouncycastle.math.ec.ECPoint;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.ECUtils;
import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>PermutationWorkerTest</code> contains tests for the class <code>{@link PermutationWorker}</code>.
 */
public class PermutationWorkerTest {

  /**
   * Run the PermutationWorker call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_1() throws Exception {
    // Create a builder specification with some dummy candidates.
    ECUtils ecUtils = new ECUtils();

    ECPoint[][] scaledCandidateCache = new ECPoint[TestParameters.CANDIDIATES][TestParameters.BLOCK_SIZE];

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.BLOCK_SIZE; blockCounter++) {
        scaledCandidateCache[candCounter][blockCounter] = ecUtils.getRandomValue().multiply(BigInteger.valueOf(blockCounter + 1));
      }
    }

    BuilderSpec builderSpec = new BuilderSpec(scaledCandidateCache, TestParameters.BLOCK_SIZE);

    ECPoint ecPoint = builderSpec.ecUtils.getRandomValue();
    byte[] cache = ecPoint.getEncoded();

    PermutationWorker fixture = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA,
        cache);

    // Test execution: with cache.
    PermutationWorker result = fixture.call();
    assertNotNull(result);
    assertEquals(fixture, result);

    ECPoint expected = ecPoint.add(builderSpec.getScaledCandidate(TestParameters.LAST_DATA[0] - 1, TestParameters.BLOCK_SIZE - 1));

    ECPoint calculated = builderSpec.ecUtils.getParams().getCurve().decodePoint(result.getData());
    assertEquals(expected, calculated);
  }

  /**
   * Run the PermutationWorker call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_2() throws Exception {
    // Create a builder specification with some dummy candidates.
    ECUtils ecUtils = new ECUtils();

    ECPoint[][] scaledCandidateCache = new ECPoint[TestParameters.CANDIDIATES][TestParameters.BLOCK_SIZE];

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.BLOCK_SIZE; blockCounter++) {
        scaledCandidateCache[candCounter][blockCounter] = ecUtils.getRandomValue().multiply(BigInteger.valueOf(blockCounter + 1));
      }
    }

    BuilderSpec builderSpec = new BuilderSpec(scaledCandidateCache, TestParameters.BLOCK_SIZE);

    PermutationWorker fixture = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA, null);

    // Test execution: without cache.
    PermutationWorker result = fixture.call();
    assertNotNull(result);
    assertEquals(fixture, result);

    ECPoint expected = builderSpec.getScaledCandidate(TestParameters.ALL_BUT_LAST_DATA[0] - 1, 0);
    expected = expected.add(builderSpec.getScaledCandidate(TestParameters.ALL_BUT_LAST_DATA[1] - 1, 1));
    expected = expected.add(builderSpec.getScaledCandidate(TestParameters.LAST_DATA[0] - 1, 2));

    ECPoint calculated = builderSpec.ecUtils.getParams().getCurve().decodePoint(result.getData());
    assertEquals(expected, calculated);
  }

  /**
   * Run the PermutationWorker(BuilderSpec,byte[],byte[],byte[]) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPermutationWorker_1() throws Exception {
    // Create a builder specification with some dummy candidates.
    ECUtils ecUtils = new ECUtils();

    ECPoint[][] scaledCandidateCache = new ECPoint[TestParameters.CANDIDIATES][TestParameters.BLOCK_SIZE];

    for (int candCounter = 0; candCounter < TestParameters.CANDIDIATES; candCounter++) {
      for (int blockCounter = 0; blockCounter < TestParameters.BLOCK_SIZE; blockCounter++) {
        scaledCandidateCache[candCounter][blockCounter] = ecUtils.getRandomValue().multiply(BigInteger.valueOf(blockCounter + 1));
      }
    }

    BuilderSpec builderSpec = new BuilderSpec(scaledCandidateCache, TestParameters.BLOCK_SIZE);

    // Test successful object creation.
    PermutationWorker result = new PermutationWorker(builderSpec, TestParameters.ALL_BUT_LAST_DATA, TestParameters.LAST_DATA, null);
    assertNotNull(result);

    assertEquals(TestParameters.ALL_BUT_LAST_DATA, result.getAllButLast());
    assertEquals(TestParameters.LAST_DATA, result.getLast());
    assertNull(result.getData());
  }
}