/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.votepacking.sort;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.votepacking.TestParameters;

/**
 * The class <code>ByteArrayComparatorTest</code> contains tests for the class <code>{@link ByteArrayComparator}</code>.
 */
public class ByteArrayComparatorTest {

  /**
   * Run the int compare(byte[],byte[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompare_1() throws Exception {
    ByteArrayComparator fixture = new ByteArrayComparator();

    byte[] a1 = new byte[TestParameters.SUB_BLOCK_SIZE];
    for (byte i = 0; i < a1.length; i++) {
      a1[i] = i;
    }

    byte[] a2 = new byte[TestParameters.SUB_BLOCK_SIZE];
    for (byte i = 0; i < a2.length; i++) {
      a2[i] = i;
    }

    byte[] a3 = new byte[TestParameters.SUB_BLOCK_SIZE * 2];
    for (byte i = 0; i < a3.length; i++) {
      a3[i] = i;
    }

    assertTrue(fixture.compare(a1, a3) < 0);

    assertEquals(0, fixture.compare(a1, a2));

    a1[4] = (byte) (a1[4] + 1); // Arbitrary index.
    assertTrue(fixture.compare(a1, a2) > 0);

    a2[4] = (byte) (a2[4] + 2); // Same index.
    assertTrue(fixture.compare(a1, a2) < 0);
  }

  /**
   * Run the int compareLimitLength(byte[],byte[],int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompareLimitLength_1() throws Exception {
    ByteArrayComparator fixture = new ByteArrayComparator();

    byte[] a1 = new byte[TestParameters.SUB_BLOCK_SIZE];
    for (byte i = 0; i < a1.length; i++) {
      a1[i] = i;
    }

    byte[] a2 = new byte[TestParameters.SUB_BLOCK_SIZE + 1];
    for (byte i = 0; i < a2.length; i++) {
      a2[i] = i;
    }

    byte[] a3 = new byte[TestParameters.SUB_BLOCK_SIZE * 2];
    for (byte i = 0; i < a3.length; i++) {
      a3[i] = i;
    }

    assertTrue(fixture.compareLimitLength(a1, a3, a3.length) < 0);

    assertEquals(0, fixture.compareLimitLength(a1, a2, a1.length));

    a1[4] = (byte) (a1[4] + 1); // Arbitrary index.
    assertTrue(fixture.compareLimitLength(a1, a2, a1.length) > 0);

    a2[4] = (byte) (a2[4] + 2); // Same index.
    assertTrue(fixture.compareLimitLength(a1, a2, a1.length) < 0);
  }

  /**
   * Run the int compareSubArrays(byte[],byte[],int) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCompareSubArrays_1() throws Exception {
    byte[] a1 = new byte[TestParameters.SUB_BLOCK_SIZE];
    for (byte i = 0; i < a1.length; i++) {
      a1[i] = i;
    }

    byte[] a2 = new byte[TestParameters.SUB_BLOCK_SIZE];
    for (byte i = 0; i < a2.length; i++) {
      a2[i] = i;
    }

    byte[] a3 = new byte[TestParameters.SUB_BLOCK_SIZE * 2];
    for (byte i = 0; i < a3.length; i++) {
      a3[i] = i;
    }

    assertTrue(ByteArrayComparator.compareSubArrays(a1, a3, a3.length + 1) < 0);

    int length = a1.length - 2; // Shorter than the actual length.
    assertEquals(0, ByteArrayComparator.compareSubArrays(a1, a2, length));

    a1[4] = (byte) (a1[4] + 1); // Arbitrary index.
    assertTrue(ByteArrayComparator.compareSubArrays(a1, a2, length) > 0);

    a2[4] = (byte) (a2[4] + 2); // Same index.
    assertTrue(ByteArrayComparator.compareSubArrays(a1, a2, length) < 0);
  }
}